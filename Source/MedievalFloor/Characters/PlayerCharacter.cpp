// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Components/CapsuleComponent.h"
#include "Camera/CameraComponent.h"
#include "../cppSceneBaseAttack.h"
#include "../cppSceneSupportSkill.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Math/Rotator.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  //Recolocar por defecto la malla del personaje centrada con la capsula y rotada hacia el forward.
  float halfCapsuleHeight = GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
  USkeletalMeshComponent* characterMesh = GetMesh();
  FVector relativeMeshLocation = characterMesh->GetRelativeLocation();
	FRotator relativeMeshRotation = characterMesh->GetRelativeRotation();
  characterMesh->SetRelativeLocation(FVector(relativeMeshLocation.X, relativeMeshLocation.Y, -halfCapsuleHeight));
	characterMesh->SetRelativeRotation(FRotator(relativeMeshRotation.Pitch, -90, relativeMeshRotation.Roll));

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->SetupAttachment(RootComponent);
	SpringArmComponent->TargetArmLength = 200.0f; 
	SpringArmComponent->bUsePawnControlRotation = true; 
	SpringArmComponent->SetRelativeLocation(FVector(0, 60, 40)); //C�mara al hombro

	ThirdPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("ThirdPersonCameraComponent"));
	ThirdPersonCameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);
	ThirdPersonCameraComponent->bUsePawnControlRotation = false;

	fOriginalCameraFOV = ThirdPersonCameraComponent->FieldOfView;
	fTargetZoomFOV = 45;
	fZoomSpeed = 10;

	fCameraLookUpRate = 45.f;
	fCameraLookRightRate = 45.f;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	pMovementComponent = GetCharacterMovement();
	pWeaponComponent = FindComponentByClass<UcppSceneBaseAttack>();
  pSupportSkillComponent = FindComponentByClass<UcppSceneSupportSkill>();

	iCurrentHealth = iCharacterHealth;
  iOriginalDefense = iCharacterDefense;
  fOriginalAttackSpeedRatio = fCharacterAttackSpeed;
  fDefaultMoveSpeedMultiplier = fMoveSpeedMultiplier;

	pMovementComponent->MaxWalkSpeed = fCharacterMoveSpeed * fMoveSpeedMultiplier;
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Funcionalidad de zoom
	if (bCanZoom && pWeaponComponent != nullptr)
	{
		if (bIsZooming && roundf(ThirdPersonCameraComponent->FieldOfView) > roundf(fTargetZoomFOV) && pWeaponComponent->isCharging)
		{
			if (fTimerToZoom < fMinTimeToZoom)
			{
				fTimerToZoom += 1 * DeltaTime;
			}
			else
			{
				float fZoomResult = FMath::Lerp(ThirdPersonCameraComponent->FieldOfView, fTargetZoomFOV, fZoomSpeed * DeltaTime);
				ThirdPersonCameraComponent->FieldOfView = fZoomResult;
			}
      if (roundf(ThirdPersonCameraComponent->FieldOfView) <= roundf(fTargetZoomFOV)) //Control para que no se pase de FOV de ida
      {
        ThirdPersonCameraComponent->FieldOfView = fTargetZoomFOV;
      }
		}
		else if (!bIsZooming && roundf(ThirdPersonCameraComponent->FieldOfView) < roundf(fOriginalCameraFOV))
		{
			fTimerToZoom = 0;
			float fZoomResult = FMath::Lerp(ThirdPersonCameraComponent->FieldOfView, fOriginalCameraFOV, fZoomSpeed * DeltaTime);
			ThirdPersonCameraComponent->FieldOfView = fZoomResult;
      if (roundf(ThirdPersonCameraComponent->FieldOfView) >= roundf(fOriginalCameraFOV)) //Control para que no se pase de FOV de vuelta
      {
        ThirdPersonCameraComponent->FieldOfView = fOriginalCameraFOV;
      }
		}
	}
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("LeftVerticalAxis", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("LeftHorizontalAxis", this, &APlayerCharacter::MoveRight);
	PlayerInputComponent->BindAxis("RightVerticalAxis", this, &APlayerCharacter::LookUp);
	PlayerInputComponent->BindAxis("RightHorizontalAxis", this, &APlayerCharacter::LookRight);

	PlayerInputComponent->BindAction("PrimaryAttackButton", IE_Pressed, this, &APlayerCharacter::PrimaryAttackControlAction);
	PlayerInputComponent->BindAction("PrimaryAttackButton", IE_Released, this, &APlayerCharacter::PrimaryAttackEndAction).bExecuteWhenPaused = true;
	PlayerInputComponent->BindAction("SecondaryAttackButton", IE_Pressed, this, &APlayerCharacter::SecondaryAttackControlAction);
	PlayerInputComponent->BindAction("JumpButton", IE_Pressed, this, &APlayerCharacter::JumpControl);
}

UcppSceneBaseAttack* APlayerCharacter::GetPlayerWeaponComponent() const
{
	return (pWeaponComponent != nullptr) ? pWeaponComponent : nullptr;
}

int APlayerCharacter::GetPlayerCurrentHealth() const
{
  return iCurrentHealth;
}

void APlayerCharacter::Damage(int _damageAmount)
{
  const int iTotalDamage = _damageAmount - iCharacterDefense;
  if (iTotalDamage > 0)
  {
    iCurrentHealth -= iTotalDamage;
    if (iCurrentHealth <= 0)
    {
      iCurrentHealth = 0;
      KillPlayer();
    }
    UE_LOG(LogTemp, Warning, TEXT("%s: %d/%d"), *GetName(), iCurrentHealth, iCharacterHealth);
    healthChangedEvent.Broadcast(this);
  }
}

float APlayerCharacter::GetAxisDeadZone() const
{
  return fInputAxisDeadZone;
}

float APlayerCharacter::GetForwardAxisValue() const
{
	return fForwardAxisValue;
}

float APlayerCharacter::GetRightAxisValue() const
{
	return fRightAxisValue;
}

float APlayerCharacter::GetLookUpAxisValue() const
{
	return fLookUpAxisValue;
}

float APlayerCharacter::GetLookRightAxisValue() const
{
	return fLookRightAxisValue;
}

void APlayerCharacter::MoveForward(float _fAxisValue)
{
  fForwardAxisValue = _fAxisValue;
  if (_fAxisValue > fInputAxisDeadZone || _fAxisValue < -fInputAxisDeadZone)
  {
    const FRotator rotation = Controller->GetControlRotation();
    const FRotator yawRotation(0, rotation.Yaw, 0);
    const FVector direction = UKismetMathLibrary::GetForwardVector(yawRotation);
    AddMovementInput(direction, _fAxisValue);
  }
}

void APlayerCharacter::MoveRight(float _fAxisValue)
{
  fRightAxisValue = _fAxisValue;
  if (_fAxisValue > fInputAxisDeadZone || _fAxisValue < -fInputAxisDeadZone)
  {
    FRotator controlRotation = GetControlRotation();
    FVector direction = UKismetMathLibrary::GetRightVector(controlRotation);
    AddMovementInput(direction, _fAxisValue);
  }
}

void APlayerCharacter::LookUp(float _fAxisValue)
{
  fLookUpAxisValue = _fAxisValue;
  if (_fAxisValue > fInputAxisDeadZone || _fAxisValue < -fInputAxisDeadZone)
  {
    AddControllerPitchInput(_fAxisValue * fCameraLookUpRate * GetWorld()->GetDeltaSeconds());
  }
}

void APlayerCharacter::LookRight(float _fAxisValue)
{
  fLookRightAxisValue = _fAxisValue;
  if (_fAxisValue > fInputAxisDeadZone || _fAxisValue < -fInputAxisDeadZone)
  {
    AddControllerYawInput(_fAxisValue * fCameraLookRightRate * GetWorld()->GetDeltaSeconds());
  }
}

void APlayerCharacter::JumpControl()
{
	if (pWeaponComponent != nullptr && !pWeaponComponent->isCharging)
	{
		Jump();
	}
}

void APlayerCharacter::PrimaryAttackControlAction()
{
	//UE_LOG(LogTemp, Warning, TEXT("Primario"));
	if (pWeaponComponent != nullptr && !pMovementComponent->IsFalling())
	{
		bIsZooming = true;
		pWeaponComponent->StartCharge();
	}
}

void APlayerCharacter::PrimaryAttackEndAction()
{
	//UE_LOG(LogTemp, Warning, TEXT("Primario End"));
	if (pWeaponComponent != nullptr && pWeaponComponent->isCharging)
	{
		bIsZooming = false;
		pWeaponComponent->Release();
	}
}

void APlayerCharacter::SecondaryAttackControlAction()
{
	//UE_LOG(LogTemp, Warning, TEXT("Secundario"));
  if (pSupportSkillComponent != nullptr)
  {
    pSupportSkillComponent->CastSupportSkill();
  }
}

void APlayerCharacter::HealPlayer(int _iHealAmount)
{
  //UE_LOG(LogTemp, Warning, TEXT("Curacion de %d puntos!"), _iHealAmount);
  iCurrentHealth += _iHealAmount;
  if (iCurrentHealth > iCharacterHealth)
  {
    iCurrentHealth = iCharacterHealth;
  }
  healthChangedEvent.Broadcast(this);
}

void APlayerCharacter::IncreasePlayerDefense(int _iDefenseToAdd, float _fEffectTime)
{
  if (!defenseModificationHandle.IsValid())
  {
    iCharacterDefense += _iDefenseToAdd;
    GetWorldTimerManager().SetTimer(defenseModificationHandle, this, &APlayerCharacter::ResetPlayerDefenseToDefault, _fEffectTime, false);
  }
}

void APlayerCharacter::IncreasePlayerMoveSpeed(float _fNewMoveSpeedMultiplier, float _fEffectTime)
{
  if (!moveSpeedModificationHandle.IsValid())
  {
    fMoveSpeedMultiplier = _fNewMoveSpeedMultiplier;
    pMovementComponent->MaxWalkSpeed = fCharacterMoveSpeed * fMoveSpeedMultiplier;
    GetWorldTimerManager().SetTimer(moveSpeedModificationHandle, this, &APlayerCharacter::ResetPlayerMoveSpeedToDefault, _fEffectTime, false);
  }
}

float APlayerCharacter::GetPlayerMoveSpeedMultiplier() const
{
  return fMoveSpeedMultiplier;
}

void APlayerCharacter::IncreasePlayerAttackSpeed(float _fRatioPercentageToSubstract, float _fEffectTime)
{
  if (!attackSpeedModificationHandle.IsValid())
  {
    float fPercentage = fCharacterAttackSpeed * _fRatioPercentageToSubstract / 100;

    fCharacterAttackSpeed -= fPercentage;
    if (fCharacterAttackSpeed < 0.2f) //Ratio minimo arbitrario
    {
      fCharacterAttackSpeed = 0.2f;
    }
    GetWorldTimerManager().SetTimer(attackSpeedModificationHandle, this, &APlayerCharacter::ResetPlayerAttackSpeedToDefault, _fEffectTime, false);
  }
}

void APlayerCharacter::ResetPlayerDefenseToDefault()
{
  iCharacterDefense = iOriginalDefense;
  GetWorldTimerManager().ClearTimer(defenseModificationHandle);
}

void APlayerCharacter::ResetPlayerMoveSpeedToDefault()
{
  fMoveSpeedMultiplier = fDefaultMoveSpeedMultiplier;
  pMovementComponent->MaxWalkSpeed = fCharacterMoveSpeed * fMoveSpeedMultiplier;
  GetWorldTimerManager().ClearTimer(moveSpeedModificationHandle);
}

void APlayerCharacter::ResetPlayerAttackSpeedToDefault()
{
  fCharacterAttackSpeed = fOriginalAttackSpeedRatio;
  GetWorldTimerManager().ClearTimer(attackSpeedModificationHandle);
}

void APlayerCharacter::KillPlayer()
{
  SetActorTickEnabled(false);

  bUseControllerRotationYaw = false;
  ThirdPersonCameraComponent->FieldOfView = fOriginalCameraFOV;
  GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

  InputComponent->AxisBindings.RemoveAt(0, 2);

  InputComponent->RemoveActionBinding(0); //Se hace resize, por lo que siempre hay que borrar el 0 pero 4 veces, por los 4 binds de acciones iniciales
  InputComponent->RemoveActionBinding(0);
  InputComponent->RemoveActionBinding(0);
  InputComponent->RemoveActionBinding(0);
  onKillPlayerDelegate.Broadcast();
}



