// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterBase.h"
#include "../../Enemies/Library/GameTeamsUtilities.h"
#include "Engine/Datatable.h"

// Sets default values
ACharacterBase::ACharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	teamId = FGenericTeamId(UGameTeamsUtilities::PlayerTeam);
}

// Called when the game starts or when spawned
void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();

	FString s;
	FCharacterStats* pMyCharacterStatsRow = pCharacterStatsTable->FindRow<FCharacterStats>(characterTypeName, s);
	
	if (pMyCharacterStatsRow)
	{
		SetCharacterHealth(pMyCharacterStatsRow->health);
		SetCharacterDefense(pMyCharacterStatsRow->defense);
		SetCharacterAttack(pMyCharacterStatsRow->attack);
		SetCharacterAttackSpeed(pMyCharacterStatsRow->attackSpeed);
		SetCharacterMoveSpeed(pMyCharacterStatsRow->moveSpeed);
	}
	else UE_LOG(LogTemp, Warning, TEXT("%s initialization failed"), *GetName());
}

// Called every frame
void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

int ACharacterBase::GetCharaterHealth() const
{
	return iCharacterHealth;
}

int ACharacterBase::GetCharaterDefense() const
{
	return iCharacterDefense;
}

int ACharacterBase::GetCharaterAttack() const
{
	return iCharacterAttack;
}

float ACharacterBase::GetCharaterAttackSpeed() const
{
	return fCharacterAttackSpeed;
}

float ACharacterBase::GetCharaterMoveSpeed() const
{
	return fCharacterMoveSpeed;
}

void ACharacterBase::SetCharacterHealth(int _iHealthAmount)
{
	iCharacterHealth = _iHealthAmount;
}

void ACharacterBase::SetCharacterDefense(int _iDefenseAmount)
{
	iCharacterDefense = _iDefenseAmount;
}

void ACharacterBase::SetCharacterAttack(int _iAttackAmount)
{
	iCharacterAttack = _iAttackAmount;
}

void ACharacterBase::SetCharacterAttackSpeed(float _fAttackSpeedAmount)
{
	fCharacterAttackSpeed = _fAttackSpeedAmount;
}

void ACharacterBase::SetCharacterMoveSpeed(float _fMoveSpeedAmount)
{
	fCharacterMoveSpeed = _fMoveSpeedAmount;
}

