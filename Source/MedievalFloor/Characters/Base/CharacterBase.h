// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Engine/Datatable.h"
#include "GenericTeamAgentInterface.h"
#include "CharacterBase.generated.h"

USTRUCT()
struct FCharacterStats : public FTableRowBase
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int health;
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int defense;
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int attack;
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float attackSpeed;
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float moveSpeed;
};

UCLASS()
class MEDIEVALFLOOR_API ACharacterBase : public ACharacter, public IGenericTeamAgentInterface
{
  GENERATED_BODY()

public:
  UPROPERTY(EditAnywhere)
    UDataTable* pCharacterStatsTable;

  //Los nombres son los tipos de personaje: Warrior, Archer, Wizard, Healer.
  UPROPERTY(EditAnywhere)
    FName characterTypeName;

protected:
  // Para definir el equipo al que pertenece el character
  FGenericTeamId teamId;

protected:
  UPROPERTY(VisibleAnywhere)
    int iCharacterHealth;

  UPROPERTY(VisibleAnywhere)
    int iCharacterDefense;

  UPROPERTY(VisibleAnywhere)
    int iCharacterAttack;

  UPROPERTY(VisibleAnywhere)
    float fCharacterAttackSpeed;

  UPROPERTY(VisibleAnywhere)
    float fCharacterMoveSpeed;

public:
	// Sets default values for this character's properties
	ACharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

  UFUNCTION(BlueprintCallable)
    int GetCharaterHealth() const;
  UFUNCTION(BlueprintCallable)
    int GetCharaterDefense() const;
  UFUNCTION(BlueprintCallable)
    int GetCharaterAttack() const;
  UFUNCTION(BlueprintCallable)
    float GetCharaterAttackSpeed() const;
  UFUNCTION(BlueprintCallable)
    float GetCharaterMoveSpeed() const;

  // Se overridea de la interfaz IGenericTeamAgentInterface
  FORCEINLINE virtual FGenericTeamId GetGenericTeamId() const override { return teamId; }

protected:
  void SetCharacterHealth(int _iHealthAmount);
  void SetCharacterDefense(int _iDefenseAmount);
  void SetCharacterAttack(int _iAttackAmount);
  void SetCharacterAttackSpeed(float _fAttackSpeedAmount);
  void SetCharacterMoveSpeed(float _fMoveSpeedAmount);

};