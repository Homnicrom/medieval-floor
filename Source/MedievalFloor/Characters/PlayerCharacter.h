// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Base/CharacterBase.h"
#include "Interfaces/IDamageable.h"
#include "Delegates/DelegateCombinations.h"
#include "PlayerCharacter.generated.h"

class UcppSceneBaseAttack;
class UcppSceneSupportSkill;
class UCharacterMovementComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthChangedEvent, APlayerCharacter*, _damagedCharacter);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnKillPlayer);

UCLASS()
class MEDIEVALFLOOR_API APlayerCharacter : public ACharacterBase, public IIDamageable
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		class USpringArmComponent* SpringArmComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		class UCameraComponent* ThirdPersonCameraComponent;

private:
	UPROPERTY() //Referencia al componente de ataque metido en la jerarquia de cada Blueprint en Unreal, cogido con FindComponent en BeginPlay
		class UcppSceneBaseAttack* pWeaponComponent;
  UPROPERTY() //Referencia al componente de habilidad en la jerarquia de cada Blueprint en Unreal, cogido con FindComponent en BeginPlay
    class UcppSceneSupportSkill* pSupportSkillComponent;
	UPROPERTY() //Referencia del componente de movimiento para uso interno en codigo de la clase
		class UCharacterMovementComponent* pMovementComponent;

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
		float fCameraLookRightRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
		float fCameraLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
		bool bCanZoom{true};
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
		float fTargetZoomFOV;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
		float fZoomSpeed;

	UPROPERTY(BlueprintAssignable)
	FHealthChangedEvent healthChangedEvent;
  UPROPERTY(BlueprintAssignable)
  FOnKillPlayer onKillPlayerDelegate;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  float fMoveSpeedMultiplier = 1.f;

private:
	UPROPERTY(VisibleAnywhere)
    int iCurrentHealth;
  int iOriginalDefense;
  float fOriginalAttackSpeedRatio;
  float fDefaultMoveSpeedMultiplier = 1.f;

  float fInputAxisDeadZone{ 0.15f };

	float fForwardAxisValue{ 0 }; //Para guardar valor del eje Forward para animaciones
	float fRightAxisValue{ 0 }; //Para guardar valor del eje Right para animaciones
	float fLookUpAxisValue{ 0 };	//Para guardar valor del eje LookUp para animaciones
	float fLookRightAxisValue{ 0 };	//Para guardar valor del eje LookRight para animaciones

	bool bIsZooming{false};
	float fMinTimeToZoom{ 0.5f }; //Tiempo minimo de mantener input para comenzar a hacer zoom si puede.
	float fTimerToZoom{ 0 }; //Timer para la variable de arriba
	float fOriginalCameraFOV;

  FTimerHandle defenseModificationHandle{};
  FTimerHandle moveSpeedModificationHandle{};
  FTimerHandle attackSpeedModificationHandle{};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	UcppSceneBaseAttack* GetPlayerWeaponComponent() const;

  UFUNCTION(BlueprintCallable)
  int GetPlayerCurrentHealth() const;

	UFUNCTION(BlueprintCallable)
	virtual void Damage(int _damageAmount) override;

  UFUNCTION(BlueprintCallable)
  float GetAxisDeadZone() const;

	UFUNCTION(BlueprintCallable)
	float GetForwardAxisValue() const;
	UFUNCTION(BlueprintCallable)
	float GetRightAxisValue() const;
	UFUNCTION(BlueprintCallable)
	float GetLookUpAxisValue() const;
	UFUNCTION(BlueprintCallable)
	float GetLookRightAxisValue() const;

	void MoveForward(float _fValue);
	void MoveRight(float _fValue);
	void LookUp(float _fValue);
	void LookRight(float _fValue);
	void JumpControl();

	void PrimaryAttackControlAction();
	void PrimaryAttackEndAction();
	void SecondaryAttackControlAction();

  UFUNCTION(BlueprintCallable)
  void HealPlayer(int _iHealAmount);
  UFUNCTION(BlueprintCallable)
  void IncreasePlayerDefense(int _iDefenseToAdd, float _fEffectTime);
  UFUNCTION(BlueprintCallable)
  void IncreasePlayerMoveSpeed(float _fNewMoveSpeedMultiplier, float _fEffectTime);
  UFUNCTION(BlueprintCallable)
  float GetPlayerMoveSpeedMultiplier() const;
  UFUNCTION(BlueprintCallable)
  void IncreasePlayerAttackSpeed(float _fRatioPercentageToSubstract, float _fEffectTime);

private:
  void ResetPlayerDefenseToDefault();
  void ResetPlayerMoveSpeedToDefault();
  void ResetPlayerAttackSpeedToDefault();

  void KillPlayer();
};
