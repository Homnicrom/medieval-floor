// Fill out your copyright notice in the Description page of Project Settings.

#include "Spawner.h"

#include "Components/ArrowComponent.h"
#include "Components/BillboardComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/Texture2D.h"

// Sets default values
ASpawner::ASpawner()
{
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp"));
	RootComponent = SceneComponent;

  SpriteComponent = CreateEditorOnlyDefaultSubobject<UBillboardComponent>(TEXT("Sprite"));
  ArrowComponent = CreateEditorOnlyDefaultSubobject<UArrowComponent>(TEXT("Arrow"));

  struct FConstructorStatics
  {
    ConstructorHelpers::FObjectFinderOptional<UTexture2D> TargetIconSpawnObject;
    ConstructorHelpers::FObjectFinderOptional<UTexture2D> TargetIconObject;
    ConstructorHelpers::FObjectFinderOptional<UParticleSystem> ParticleSystemObject;
    FName ID_TargetPoint;
    FText NAME_TargetPoint;

    FConstructorStatics() : TargetIconSpawnObject(TEXT("/Engine/EditorMaterials/TargetIconSpawn"))
                            , TargetIconObject(TEXT("/Engine/EditorMaterials/TargetIcon"))
                            , ParticleSystemObject(TEXT("/Game/Developers/mrurk/Collections/FX/Spawner_Effect_Particles.Spawner_Effect_Particles"))
                            , ID_TargetPoint(TEXT("TargetPoint"))
                            , NAME_TargetPoint(NSLOCTEXT("SpriteCategory", "TargetPoint", "Target Points"))
    {
    }
  };
  static FConstructorStatics ConstructorStatics;

  if (SpriteComponent)
  {
    SpriteComponent->Sprite = ConstructorStatics.TargetIconObject.Get();
    SpriteComponent->SetRelativeScale3D_Direct(FVector(0.35f, 0.35f, 0.35f));
    //SpriteComponent->SpriteInfo.Category = ConstructorStatics.ID_TargetPoint;
    //SpriteComponent->SpriteInfo.DisplayName = ConstructorStatics.NAME_TargetPoint;
    SpriteComponent->bIsScreenSizeScaled = true;

    SpriteComponent->SetupAttachment(SceneComponent);
  }

  if (ArrowComponent)
  {
    ArrowComponent->ArrowColor = FColor(150, 200, 255);

    ArrowComponent->ArrowSize = 0.5f;
    ArrowComponent->bTreatAsASprite = true;
    //ArrowComponent->SpriteInfo.Category = ConstructorStatics.ID_TargetPoint;
    //ArrowComponent->SpriteInfo.DisplayName = ConstructorStatics.NAME_TargetPoint;
    ArrowComponent->SetupAttachment(SceneComponent);
    ArrowComponent->bIsScreenSizeScaled = true;

    // Counteract the scaled down parent so that the arrow is large enough to see.
    if (SpriteComponent)
    {
      ArrowComponent->SetRelativeScale3D((FVector::OneVector / SpriteComponent->GetRelativeScale3D()));
    }
  }

  ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystem"));
  if (ParticleSystemComponent)
  {
    ParticleSystemComponent->SetupAttachment(SceneComponent);
    ParticleSystemComponent->Template = ConstructorStatics.ParticleSystemObject.Get();
    ParticleSystemComponent->bAutoActivate = false;
  }
}

void ASpawner::EnableSpawner(bool _enabled)
{
  if(ParticleSystemComponent)
  {
    if(_enabled)
    {
      ParticleSystemComponent->Activate();
    }
    else
    {
      ParticleSystemComponent->Deactivate();
    }
  }
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
  Super::BeginPlay();
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
}
