// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/InputComponent.h"
#include "ControllerUtil.generated.h"

UCLASS()
class MEDIEVALFLOOR_API UControllerUtil : public UObject
{
  GENERATED_BODY()

public:
  UPROPERTY()
  APlayerController* PlayerController;

  FInputActionBinding ConfirmBinding;
  FInputActionBinding BackBinding;
  FInputActionBinding PauseBinding;

  template <typename UObjectTemplate>
  static FInputActionBinding CreateOneParamBinding(UObjectTemplate* _userObject, int _controllerID, FName _actionName, FName _funcName)
  {
    //Declarar action binding
    FInputActionBinding ActionBindingOneParam(_actionName, IE_Pressed);
    //Crear el handler que bindea la acci�n con la funci�n indicada
    FInputActionHandlerSignature OneParamActionHandler;
    //Bindear la funci�n a nuestro handler. UserObject es la clase donde se buscar� la funci�n
    OneParamActionHandler.BindUFunction(_userObject, _funcName, _controllerID);
    //Asociar el action binding con el nuevo delegate
    ActionBindingOneParam.ActionDelegate = OneParamActionHandler;
    return ActionBindingOneParam;
  }
};
