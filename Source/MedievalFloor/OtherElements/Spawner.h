// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/SceneComponent.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class MEDIEVALFLOOR_API ASpawner : public AActor
{
  GENERATED_BODY()

public:
  // Sets default values for this actor's properties
  ASpawner();

  void EnableSpawner(bool _enabled);

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = VisualEffect)
  UParticleSystemComponent* ParticleSystemComponent;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
  USceneComponent* SceneComponent;

private:
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Display, meta = (AllowPrivateAccess = "true"))
  class UBillboardComponent* SpriteComponent;

  UPROPERTY()
  class UArrowComponent* ArrowComponent;

protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

public:
  // Called every frame
  virtual void Tick(float DeltaTime) override;
};
