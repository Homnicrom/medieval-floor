// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../Characters/Base/CharacterBase.h"
#include "../../Characters/Interfaces/IDamageable.h"
#include "../Library/MeshMergeFunctionLibrary.h"
#include "EnemiesBase.generated.h"

// Forward declaration
class USkeletalMeshComponent;
class UStaticMeshComponent;
class USceneComponent;
class UcppSceneBaseAttack;

// Data table
USTRUCT()
struct FEnemyStats : public FCharacterStats
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  float attackProbability;
};

// Enemy types
UENUM()
enum EEnemyType
{
  Weak, Archer,
  Fast, Big
};
USTRUCT()
struct FEnemyType
{
  GENERATED_USTRUCT_BODY()

  static FName AsName(EEnemyType _eEnum);
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEnemyDamaged, AEnemiesBase*, _damagedEnemy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEnemyDeadEventSignature, AEnemiesBase*, _deadEnemy);

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API AEnemiesBase : public ACharacterBase, public IIDamageable
{
	GENERATED_BODY()
	
protected:

	FName enemyTag;
  int iEnemyLevel;

  /**
  * @brief Timer para eliminacion de la instancia tras la muerte
  */
  FTimerHandle dieTimerHandle;
  /**
  * @brief Vida en tiempo de juego
  */
  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (DisplayName = "Current Health"))
    int iCurrentHealth;
  /**
  * @brief Vida en tiempo de juego
  */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (DisplayName = "Attack probability"))
    float fAttackProbability;

public:
  /**
  * @brief Delegate para eventos de da�o
  */
  UPROPERTY(BlueprintAssignable)
  FOnEnemyDamaged damageEvent;
  /**
  * @brief Delegate para eventos de muerte de enemigo
  */
  UPROPERTY(BlueprintAssignable)
  FOnEnemyDeadEventSignature OnEnemyDeadEventDelegate;


  /**
  * @brief Variable para definir los modulos del skeletal mesh
  */ 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Modular Skeletal Mesh")
		FSkeletalMeshMergeParams modularSkeletalMeshParams;
  /**
  * @brief Crear el skeletal mesh a partir de los modulos por defecto
  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Modular Skeletal Mesh")
		bool bMergeModularSkeletalMeshByDefault;
  /**
  * DEPRECATED
  */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh", meta = (DisplayName = "Weapon static mesh component"))
	//	UStaticMeshComponent* weaponMeshComponent;
  /**
  * @brief Componente de ataque
  */
  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attack vars", meta = (DisplayName = "Weapon component"))
    UcppSceneBaseAttack* pWeaponComponent;


public:

	// Sets default values
	AEnemiesBase();

protected:
	// Called when the game starts or enemy is spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		FORCEINLINE FName GetEnemyTag() const { return enemyTag; }

	UFUNCTION(BlueprintCallable)
		float GetForwardSpeed() const;

  UFUNCTION(BlueprintCallable)
    float GetRightSpeed() const;

  UFUNCTION(BlueprintCallable, Category = "Get")
    int GetCurrentHealth() const;
  UFUNCTION(BlueprintCallable, Category = "Get")
    int GetMaxHealth() const;

  int GetEnemyLevel() const;
  void SetEnemyLevel(int _level);

  FORCEINLINE float GetAttackProbability() const { return fAttackProbability; }


  UFUNCTION(BlueprintCallable, Category = "Get")
    UcppSceneBaseAttack* GetWeaponComponent() const;
  


  UFUNCTION(BlueprintCallable, Category = "Action|Attack")
    void BeginAttack();
  UFUNCTION(BlueprintCallable, Category = "Action|Attack")
    void EndAttack();
  UFUNCTION(BlueprintCallable)
    virtual void Damage(int _iDamageAmount) override;

protected:

  void Die();
  void TerminateEnemy();
  
};
