// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemiesBase.h"
#include "DistanceCombatEnemy.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API ADistanceCombatEnemy : public AEnemiesBase
{
	GENERATED_BODY()

public:

	ADistanceCombatEnemy();
	
};
