// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemiesBase.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BrainComponent.h"
#include "AIController.h"
#include "MedievalFloor/Enemies/Controllers/AIEnemyControllerBase.h"
#include "MedievalFloor/cppSceneBaseAttack.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/SkeletalMesh.h"
#include "Engine/Datatable.h"
#include "Components/CapsuleComponent.h"


AEnemiesBase::AEnemiesBase()
{
  // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  enemyTag = TEXT("Enemy");
  Tags.Add(enemyTag);

  // Deprecated
  //weaponMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon static mesh component"));
  //weaponMeshComponent->SetupAttachment(GetMesh(), TEXT("RightWeaponShield"));

  GetCapsuleComponent()->SetCapsuleHalfHeight(95.f);
  GetCapsuleComponent()->SetCapsuleRadius(40.f);
  bMergeModularSkeletalMeshByDefault = true;
}

void AEnemiesBase::BeginPlay()
{
  Super::BeginPlay();

  if (bMergeModularSkeletalMeshByDefault)
  {
    USkeletalMesh* skeletalMesh = UMeshMergeFunctionLibrary::MergeMeshes(modularSkeletalMeshParams);
    ensureAlways(skeletalMesh != nullptr);
    GetMesh()->SetSkeletalMesh(skeletalMesh);
  }
  pWeaponComponent = FindComponentByClass<UcppSceneBaseAttack>();
  iCurrentHealth = iCharacterHealth;

  // Lectura de stat
  FEnemyStats* pEnemyStats = pCharacterStatsTable->FindRow<FEnemyStats>(characterTypeName, "");
  ensure(pEnemyStats != nullptr);
  if (pEnemyStats != nullptr)
  {
    fAttackProbability = pEnemyStats->attackProbability;
  }
  GetCharacterMovement()->MaxWalkSpeed = pEnemyStats->moveSpeed;
}

void AEnemiesBase::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
}

float AEnemiesBase::GetForwardSpeed() const
{
  return GetCharacterMovement()->GetLastUpdateVelocity().Size();
}

float AEnemiesBase::GetRightSpeed() const
{
  return GetCharacterMovement()->GetLastInputVector().Y;
}

UcppSceneBaseAttack* AEnemiesBase::GetWeaponComponent() const
{
  return pWeaponComponent;
}

int AEnemiesBase::GetCurrentHealth() const 
{
  return iCurrentHealth; 
}

int AEnemiesBase::GetMaxHealth() const
{
  return iCharacterHealth;
}

void AEnemiesBase::BeginAttack()
{
  if (pWeaponComponent != nullptr && !GetCharacterMovement()->IsFalling() && !pWeaponComponent->isCharging)
  {
    AAIController* controller = Cast<AAIController>(GetController());
    if (controller != nullptr)
    {
      controller->GetBlackboardComponent()->SetValueAsBool(TEXT("IsAttackInProcess"), true);
    }
    pWeaponComponent->StartCharge();
  }
}

void AEnemiesBase::EndAttack()
{
  if (pWeaponComponent != nullptr && pWeaponComponent->isCharging)
  {
    AAIController* controller = Cast<AAIController>(GetController());
    if (controller != nullptr)
    {
      controller->GetBlackboardComponent()->SetValueAsBool(TEXT("IsAttackInProcess"), false);
    }
    pWeaponComponent->Release();
  }
}

void AEnemiesBase::SetEnemyLevel(int _level)
{
  iEnemyLevel = _level;
}

int AEnemiesBase::GetEnemyLevel() const
{
  return iEnemyLevel;
}

void AEnemiesBase::Damage(int _iDamageAmount)
{
  int totalDamage = _iDamageAmount - iCharacterDefense;
  if (totalDamage < 0)
  {
    return;
  }
  iCurrentHealth -= totalDamage;
  if (iCurrentHealth < 0)
  {
    iCurrentHealth = 0;
    Die();
  }
  // @debug
  UE_LOG(LogTemp, Warning, TEXT("%s was damaged. Current Health: %d"), *GetName(), iCurrentHealth);
  damageEvent.Broadcast(this);
}

void AEnemiesBase::Die()
{
  OnEnemyDeadEventDelegate.Broadcast(this);
  AAIController* controller = Cast<AAIController>(GetController());
  ensure(controller != nullptr);
  controller->BrainComponent->StopLogic(TEXT("Enemy dies"));
  GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
  GetWorldTimerManager().SetTimer(dieTimerHandle, this, &AEnemiesBase::TerminateEnemy, 10.f, false);
}

void AEnemiesBase::TerminateEnemy()
{
  Destroy();
}


// AUX

FName FEnemyType::AsName(EEnemyType _eEnum)
{
  switch (_eEnum)
  {
  case EEnemyType::Weak:   return FName(TEXT("Weak"));
  case EEnemyType::Archer: return FName(TEXT("Archer"));;
  case EEnemyType::Fast:   return FName(TEXT("Fast"));;
  case EEnemyType::Big:    return FName(TEXT("Big"));;
  }
  return FName();
}
