// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemiesBase.h"
#include "NearCombatEnemy.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API ANearCombatEnemy : public AEnemiesBase
{
	GENERATED_BODY()
	
public:

  /**
  * DEPRECATED
  */
	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh", meta = (DisplayName = "Shield static mesh component"))
		UStaticMeshComponent* shieldMeshComponent;*/

public:

	ANearCombatEnemy();
};
