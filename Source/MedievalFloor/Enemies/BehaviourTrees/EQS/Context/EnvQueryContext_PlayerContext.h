// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryContext.h"
#include "EnvQueryContext_PlayerContext.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UEnvQueryContext_PlayerContext : public UEnvQueryContext
{
	GENERATED_BODY()
	
public:

  /**
  * @brief Modifica el context del EQS con el detected player especificado en el blackboard
  */
  virtual void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const override;

};
