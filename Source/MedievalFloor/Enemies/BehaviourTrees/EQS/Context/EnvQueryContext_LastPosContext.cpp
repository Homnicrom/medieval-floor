// Fill out your copyright notice in the Description page of Project Settings.


#include "EnvQueryContext_LastPosContext.h"
#include "MedievalFloor/Enemies/Controllers/AIEnemyControllerBase.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Point.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "BehaviorTree/BlackboardComponent.h"

void UEnvQueryContext_LastPosContext::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
  Super::ProvideContext(QueryInstance, ContextData);

  AActor* actor = Cast<AActor>(QueryInstance.Owner);
  ensure(actor != nullptr);
  AAIEnemyControllerBase* controller = Cast<AAIEnemyControllerBase>(actor->GetInstigatorController());
  ensure(controller != nullptr);
  UBlackboardComponent* blackboard = controller->GetBlackboardComponent();
  
  UEnvQueryItemType_Point::SetContextHelper(ContextData, blackboard->GetValueAsVector(TEXT("LastKnownPosition")));
}
