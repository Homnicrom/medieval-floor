// Fill out your copyright notice in the Description page of Project Settings.


#include "EnvQueryContext_PlayerContext.h"
#include "MedievalFloor/Enemies/Controllers/AIEnemyControllerBase.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "BehaviorTree/BlackboardComponent.h"

void UEnvQueryContext_PlayerContext::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
  Super::ProvideContext(QueryInstance, ContextData);

  AActor* actor = Cast<AActor>(QueryInstance.Owner);
  ensure(actor != nullptr);
  AAIEnemyControllerBase* controller = Cast<AAIEnemyControllerBase>(actor->GetInstigatorController());
  ensure(controller != nullptr);
  UBlackboardComponent* blackboard = controller->GetBlackboardComponent();
  AActor* detectedPlayer = Cast<AActor>(blackboard->GetValueAsObject(controller->targetPlayerBBReference));
  ensure(detectedPlayer != nullptr);
  UEnvQueryItemType_Actor::SetContextHelper(ContextData, detectedPlayer);
}