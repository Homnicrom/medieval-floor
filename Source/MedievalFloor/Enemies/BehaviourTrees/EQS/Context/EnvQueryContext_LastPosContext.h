// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryContext.h"
#include "EnvQueryContext_LastPosContext.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UEnvQueryContext_LastPosContext : public UEnvQueryContext
{
	GENERATED_BODY()
	
  /**
  * @brief Modificar el contexto del sistema EQS a la ultima posicion conocida dada por un valor del Blackboard
  */
  virtual void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const override;
};
