// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_CheckDistance.h"
#include "BehaviorTree/BlackboardComponent.h"

void UBTService_CheckDistance::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
  ensure(!bbKeyDistanceToPlayer.SelectedKeyName.IsNone());
  ensure(!bbKeyDetectedPlayer.SelectedKeyName.IsNone());

  // Get our location
  FVector enemyLocation = OwnerComp.GetOwner()->GetActorLocation();

  // Get player location
  UBlackboardComponent* blackboard = OwnerComp.GetBlackboardComponent();
  ensure(blackboard != nullptr);
  if (blackboard)
  {
    AActor* player = Cast<AActor>(blackboard->GetValueAsObject(bbKeyDetectedPlayer.SelectedKeyName));
    //ensure(player != nullptr);
    // Sometimes the service is called without a detected player in the blackboard. It should be checked
    if (player)
    {
      FVector playerLocation = player->GetActorLocation();
      // Set value in blackboard
      float fDistanceToPlayer = (playerLocation - enemyLocation).Size();
      blackboard->SetValueAsFloat(bbKeyDistanceToPlayer.SelectedKeyName, fDistanceToPlayer);
    }
  }

}
