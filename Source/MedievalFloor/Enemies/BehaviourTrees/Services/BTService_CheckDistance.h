// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "BTService_CheckDistance.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UBTService_CheckDistance : public UBTService_BlackboardBase
{
	GENERATED_BODY()

public:

  /**
  * @brief Blackboard key de referencia para guardar la distancia al jugador
  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Blackboard", meta = (DisplayName = "Distance player blackboard key"))
		FBlackboardKeySelector bbKeyDistanceToPlayer;
  /**
  * @brief Blakboard key de referencia del jugador detectado
  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Blackboard", meta = (DisplayName = "Detected player blackboard key"))
		FBlackboardKeySelector bbKeyDetectedPlayer;

public:

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	virtual void OnGameplayTaskActivated(UGameplayTask& Task) override {}
	virtual void OnGameplayTaskDeactivated(UGameplayTask& Task) override {}
	
};
