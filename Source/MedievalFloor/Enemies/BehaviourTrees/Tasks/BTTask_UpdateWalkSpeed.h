// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_UpdateWalkSpeed.generated.h"


/**
* @brief Enum para seleccionar la actualizacion de velocidad
*/
UENUM()
	enum SpeedType
{
	Stopped,
	Patrol,
	Chase
};

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UBTTask_UpdateWalkSpeed : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed selection", meta = (DisplayName = "Speed selection"))
		TEnumAsByte<SpeedType> eSpeedSelector;

public:


	virtual void OnGameplayTaskActivated(UGameplayTask& Task) override {}
	virtual void OnGameplayTaskDeactivated(UGameplayTask& Task) override {}

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

};
