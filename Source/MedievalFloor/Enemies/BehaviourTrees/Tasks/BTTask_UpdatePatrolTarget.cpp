// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_UpdatePatrolTarget.h"
#include "NavigationSystem.h"
#include "../../Controllers/AIEnemyControllerBase.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTTask_UpdatePatrolTarget::UBTTask_UpdatePatrolTarget()
{
  fPatrolRadius = 1000.f;
}


EBTNodeResult::Type UBTTask_UpdatePatrolTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
  // Change blackboard value
  ensure(!bbKeyPatrolLocation.SelectedKeyName.IsNone());
  if (bbKeyPatrolLocation.SelectedKeyName.IsNone())
  {
    return EBTNodeResult::Failed;
  }
  FVector currentControllerPosition = OwnerComp.GetOwner()->GetActorLocation();
  FVector nextPatrolLocation = UNavigationSystemV1::GetRandomReachablePointInRadius(this, currentControllerPosition, fPatrolRadius);
  UBlackboardComponent* blackboard = OwnerComp.GetBlackboardComponent();
  ensure(blackboard != nullptr);
  if (blackboard == nullptr)
  {
    return EBTNodeResult::Failed;
  }
  blackboard->SetValueAsVector(bbKeyPatrolLocation.SelectedKeyName, nextPatrolLocation);

  return EBTNodeResult::Type::Succeeded;
}
