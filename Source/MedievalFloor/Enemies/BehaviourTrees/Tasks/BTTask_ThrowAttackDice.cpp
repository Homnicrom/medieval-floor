// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_ThrowAttackDice.h"
#include "../../Controllers/AIEnemyControllerBase.h"
#include "../../EnemyClass/EnemiesBase.h"
#include "Math/UnrealMathUtility.h"

EBTNodeResult::Type UBTTask_ThrowAttackDice::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
  AAIEnemyControllerBase* controller = Cast<AAIEnemyControllerBase>(OwnerComp.GetOwner());
  ensure(controller != nullptr);
  AEnemiesBase* enemy = Cast<AEnemiesBase>(controller->GetPawn());
  ensure(enemy != nullptr);
  float fDice = FMath::RandRange(0.f, 1.f);
  if (fDice <= enemy->GetAttackProbability())
  {
    return EBTNodeResult::Succeeded;
  }
  else
  {
    return EBTNodeResult::Failed;
  }
}