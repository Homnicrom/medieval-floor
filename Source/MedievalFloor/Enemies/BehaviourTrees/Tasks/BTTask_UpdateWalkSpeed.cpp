// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_UpdateWalkSpeed.h"
#include "../../Controllers/AIEnemyControllerBase.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"

EBTNodeResult::Type UBTTask_UpdateWalkSpeed::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
  // Set patrol speed
  AAIEnemyControllerBase* controller = Cast<AAIEnemyControllerBase>(OwnerComp.GetOwner());

  // DEPRECATED
  //if (controller == nullptr)
  //{
  //  return EBTNodeResult::Failed;
  //}
  //float fUpdatedSpeed = 0.f;
  //switch (eSpeedSelector)
  //{
  //case SpeedType::Chase:
  //  fUpdatedSpeed = enemy->GetChaseSpeed();
  //  break;
  //case SpeedType::Patrol:
  //  fUpdatedSpeed = controller->GetPatrolSpeed();
  //}
  //ACharacter* character = Cast<ACharacter>(controller->GetPawn());
  //if (character == nullptr)
  //{
  //  return EBTNodeResult::Failed;
  //}
  //character->GetCharacterMovement()->MaxWalkSpeed = fUpdatedSpeed;
  return EBTNodeResult::Succeeded;
}