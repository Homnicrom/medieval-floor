// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_CheckEnemyPosition.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UBTTask_CheckEnemyPosition::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
  ensure(!bbKeyDetectedPlayer.SelectedKeyName.IsNone());
  if (bbKeyDetectedPlayer.SelectedKeyName.IsNone())
  {
    return EBTNodeResult::Type::Aborted;
  }

  UBlackboardComponent* blackboard = Cast<UBlackboardComponent>(OwnerComp.GetBlackboardComponent());
  ensure(blackboard != nullptr);
  AActor* player = Cast<AActor>(blackboard->GetValueAsObject(bbKeyDetectedPlayer.SelectedKeyName));
  ensure(player != nullptr);
  AActor* enemy = Cast<AActor>(OwnerComp.GetOwner());

  FVector forwardVector = player->GetActorForwardVector();
  FVector enemyVector = enemy->GetActorLocation() - player->GetActorLocation();

  if (FVector::DotProduct(forwardVector, enemyVector) <= 0)
  {
    return EBTNodeResult::Type::Failed;
  }
  else return EBTNodeResult::Type::Succeeded;
}
