// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_Attack.h"
#include "../../EnemyClass/EnemiesBase.h"
#include "AIController.h"
#include "BTTask_ThrowAttackDice.h"


EBTNodeResult::Type UBTTask_Attack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
  AAIController* controller = Cast<AAIController>(OwnerComp.GetOwner());
  ensure(controller != nullptr);
  AEnemiesBase* enemy = Cast<AEnemiesBase>(controller->GetPawn());
  ensure(enemy != nullptr);
  enemy->BeginAttack();
  return EBTNodeResult::Succeeded;
}