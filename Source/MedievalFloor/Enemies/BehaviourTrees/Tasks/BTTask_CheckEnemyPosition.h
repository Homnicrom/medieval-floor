// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_CheckEnemyPosition.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UBTTask_CheckEnemyPosition : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:

  /**
  * @brief Blakboard key de referencia del jugador detectado
  */
  UPROPERTY(EditAnywhere, Category = "Blackboard", meta = (DisplayName = "Detected player blackboard key"))
    FBlackboardKeySelector bbKeyDetectedPlayer;

public:

  virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

  virtual void OnGameplayTaskActivated(UGameplayTask& Task) override {}
  virtual void OnGameplayTaskDeactivated(UGameplayTask& Task) override {}
	
};
