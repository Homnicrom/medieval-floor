// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_UpdatePatrolTarget.generated.h"

/**
* DEPRECATED
* ----------
* Los enemigos van directamente a por los jugadores, por lo que no se utiliza la 
* tarea de patrol.
*/


/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UBTTask_UpdatePatrolTarget : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Patrol config", meta = (ClampMin="0.0", ClampMax="1000.0"))
		float fPatrolRadius;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Blackboard", meta = (DisplayName = "Patrol location Blackboard key"))
		FBlackboardKeySelector bbKeyPatrolLocation;
	
public:

	UBTTask_UpdatePatrolTarget();

	virtual void OnGameplayTaskActivated(UGameplayTask& Task) override {}
	virtual void OnGameplayTaskDeactivated(UGameplayTask& Task) override {}

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
