// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/UserDefinedEnum.h"
#include "GameTeamsUtilities.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UGameTeamsUtilities : public UUserDefinedEnum
{
	GENERATED_BODY()
	
public:

	enum EGameTeams
	{
		PlayerTeam,
		EnemyTeam
	};
};
