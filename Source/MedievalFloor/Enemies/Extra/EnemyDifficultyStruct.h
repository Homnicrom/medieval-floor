// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MedievalFloor/Enemies/EnemyClass/EnemiesBase.h"

#include "EnemyDifficultyStruct.generated.h"

USTRUCT(BlueprintType)
struct MEDIEVALFLOOR_API FEnemyDifficultyStruct
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  TSubclassOf<AEnemiesBase> Enemy;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  uint8 DifficultyLevel;

  bool operator<(const FEnemyDifficultyStruct& _other) const
  {
    return DifficultyLevel < _other.DifficultyLevel;
  }
};
