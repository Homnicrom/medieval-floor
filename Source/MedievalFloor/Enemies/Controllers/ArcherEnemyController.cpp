// Fill out your copyright notice in the Description page of Project Settings.


#include "ArcherEnemyController.h"
#include "Math/UnrealMathUtility.h"
#include "../../Characters/Base/CharacterBase.h"
#include "../../Characters/PlayerCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"

AArcherEnemyController::AArcherEnemyController()
{
  PrimaryActorTick.bCanEverTick = true;
  // DEPRECATED
  //OnSightUpdated.AddDynamic(this, &AArcherEnemyController::SightSenseUpdate);
}

void AArcherEnemyController::BeginPlay()
{
  Super::BeginPlay();
  // ...
}

void AArcherEnemyController::SightSenseUpdate(const TArray<APlayerCharacter*>& UpdatedPlayerList)
{
  ChoosePlayer_Internal(UpdatedPlayerList);
}

void AArcherEnemyController::ChoosePlayer()
{
  TArray<APlayerCharacter*> playerList = GetFilteredPlayerList(GetDetectedPlayers());
  ChoosePlayer_Internal(playerList);
}

void AArcherEnemyController::ChoosePlayer_Internal(const TArray<APlayerCharacter*>& UpdatedPlayerList)
{
  if (UpdatedPlayerList.Num() != 0)
  {
    ClearTargetPlayer();
    // Get random player
    int32 iRandomPlayerIndex = FMath::RandRange(0, UpdatedPlayerList.Num() - 1);
    // Bind player killed event to ChoosePlayer
    UpdatedPlayerList[iRandomPlayerIndex]->onKillPlayerDelegate.AddDynamic(this, &AAIEnemyControllerBase::ChoosePlayer);
    SetTargetPlayer(UpdatedPlayerList[iRandomPlayerIndex]);
  }
}