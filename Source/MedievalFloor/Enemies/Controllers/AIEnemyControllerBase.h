// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BehaviorTree/BehaviorTreeTypes.h"
#include "Engine/Datatable.h"
#include "AIEnemyControllerBase.generated.h"


// Forward declarations ----
class APlayerCharacter;


// Delegate -----------------
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSightUpdatedDelegate, const TArray<APlayerCharacter*>&, UpdatedPlayerList);

/**
 *
 */
UCLASS()
class MEDIEVALFLOOR_API AAIEnemyControllerBase : public AAIController
{
	GENERATED_BODY()

protected:
  /**
  * DEPRECATED
  * @brief Delegate para la actualizacion de actores en el campo de vision. Las clases hijas se suscriben al delegate para recibir
  * la lista de actores en el campo de vision.
  */
	FSightUpdatedDelegate OnSightUpdated;

 // /**
 // * @brief Velocidad para perseguir al jugador
 // */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behaviour against player", meta = (ClampMin = "0.0", ClampMax = "1000.0", DisplayName = "Chase speed"))
	//	float fChaseSpeed;
 // /**
 // * DEPRECATED
 // * @brief Velocidad de patrullaje
 // */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behaviour against player", meta = (ClampMin = "0.0", ClampMax = "250.0", DisplayName = "Patrol speed"))
	//	float fPatrolSpeed;


  /**
  * @brief Probabilidad de ataque del enemigo
  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behaviour against player", meta = (ClampMin = "0.0", ClampMax = "1.0", DisplayName = "Attack probability"))
		float fAttackProbability;

public:

  /**
  * DEPRECATED
  * @brief Componente de percepcion.
  */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI|Perception")
		class UAIPerceptionComponent* pAIPerceptionComponent;
  /**
  * DEPRECATED
  * @brief Componente de vista para la percepcion.
  */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI|Perception")
		class UAISenseConfig_Sight* pSightPerceptionComponent;
  /**
  * DEPRECATED
  * @brief Nombre en blackboard del campo donde se guarda la ultima posicion conocida del jugador
  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|BehaviourTree|Blackboard", meta = (DisplayName = "Name last known position bb reference"))
		FName lastKnownPositionBBReference;

  /**
  * @brief Asset del behavior tree que se desea ejecutar en el controlador.
  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|BehaviourTree")
		class UBehaviorTree* pBehaviorTreeAsset;
  /**
  * @brief Nombre en blackboard del campo donde se guarda al jugador objetivo
  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI|BehaviourTree|Blackboard", meta = (DisplayName = "Name target player bb reference"))
		FName targetPlayerBBReference;
	/**
  * @brief Asset de la tabla de datos del controlador
  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Controller|Stats", meta = (DisplayName = "Enemy controller stats table"))
		UDataTable* pEnemyControllerStatsTable;

  // DEPRECATED
 // /**
 // * @brief Selector de tipo de enemigo controlado.
 // */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Controller|Stats", meta = (DisplayName = "Enemy type selector"))
	//	TEnumAsByte<EEnemyType> enemyTypeSelector;
	

private:
  /**
  * DEPRECATED
  * @brief Identificacion del equipo
  */
	FGenericTeamId teamId;
  /**
  * DEPRECATED
  * @brief Lista de jugadores al alcance
  */
	TArray<APlayerCharacter*> detectedPlayersArray;

public:

	// Sets default values for controller's properties
	AAIEnemyControllerBase();

protected:

	// Called when the actor is spawned or the game starts
	virtual void BeginPlay() override;

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnPossess(APawn* _pPawn) override;

  // DEPRECATED
	//FORCEINLINE float GetAttackProbability() const { return fAttackProbability; }
 // FORCEINLINE void SetAttackProbability(float attackProbability) { fAttackProbability = attackProbability; }

  // DEPRECATED
	//FORCEINLINE float GetPatrolSpeed() const { return fPatrolSpeed; }
	//FORCEINLINE float GetChaseSpeed() const { return fChaseSpeed; }

	// Team system evaluator
	virtual ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;
	FORCEINLINE virtual FGenericTeamId GetGenericTeamId() const override { return teamId; }

	// Returns the current detected players by the ai perception
  const TArray<APlayerCharacter*>& GetDetectedPlayers() const;

  /**
  * @brief Modificar el jugador objectivo en el blackboard
  */
  void SetTargetPlayer(APlayerCharacter* player);
  /**
  * @brief Devuelve el actual jugador fijado
  */
  APlayerCharacter* GetTargetPlayer() const;
  /**
  * @brief Limpiar los enlaces con el jugador objetivo y limpiar la variable de blackboard
  */
  void ClearTargetPlayer();

  /**
  * DEPRECATED
  * @brief Delegate para filtrado de actores en vision
  */
	UFUNCTION(Category = "AI|Perception|Sight")
		void DefaultSightSenseUpdate(const TArray<AActor*>& UpdatedActors);

  /**
  * @brief Metodo que tienen que sobreescribir las clases hijas para la eleccion del objetivo
  */
  virtual void ChoosePlayer();

  /**
  * @brief Delegate para actualizar el jugador objetivo cuando el actual lance el evento de que ha muerto
  */
  UFUNCTION()
    void UpdateTargetPlayer();

protected:

  /**
  * @brief Filtrado de la lista de jugadores
  */
  static TArray<APlayerCharacter*> GetFilteredPlayerList(const TArray<APlayerCharacter*>& UpdatedPlayerList);

private:
  /**
  * @brief Inicializacion de variables desde la tabla de datos
  */
	void InitStatsFromDataTable();
  /**
  * DEPRECATED
  * @brief Actualizacion de detectedPlayerList con los datos de la percepcion
  */
  void UpdatePlayerArray(const TArray<AActor*>& UpdatedActors);
};
