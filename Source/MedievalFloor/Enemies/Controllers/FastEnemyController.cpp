// Fill out your copyright notice in the Description page of Project Settings.


#include "FastEnemyController.h"
#include "Math/UnrealMathUtility.h"
#include "../../Characters/Base/CharacterBase.h"
#include "../../Characters/PlayerCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"

AFastEnemyController::AFastEnemyController()
{
  PrimaryActorTick.bCanEverTick = true;
  // DEPRECATED
  //OnSightUpdated.AddDynamic(this, &AFastEnemyController::SightSenseUpdate);
}

void AFastEnemyController::BeginPlay()
{
  Super::BeginPlay();
}

void AFastEnemyController::SightSenseUpdate(const TArray<APlayerCharacter*>& UpdatedPlayerList)
{
  ChoosePlayer_Internal(UpdatedPlayerList);
}

void AFastEnemyController::ChoosePlayer()
{
  ChoosePlayer_Internal(GetDetectedPlayers());
}

void AFastEnemyController::ChoosePlayer_Internal(const TArray<APlayerCharacter*>& UpdatedPlayerList)
{
  if (UpdatedPlayerList.Num() != 0)
  {
    ClearTargetPlayer();
    int32 iMinHealthPlayerIndex = 0;
    for (int32 iIndex = 0; iIndex < UpdatedPlayerList.Num(); ++iIndex)
    {
      if (UpdatedPlayerList[iIndex]->GetPlayerCurrentHealth() > 0 && 
        UpdatedPlayerList[iIndex]->GetPlayerCurrentHealth() < UpdatedPlayerList[iMinHealthPlayerIndex]->GetPlayerCurrentHealth())
      {
        iMinHealthPlayerIndex = iIndex;
      }
    }
    UpdatedPlayerList[iMinHealthPlayerIndex]->onKillPlayerDelegate.AddDynamic(this, &AAIEnemyControllerBase::ChoosePlayer);
    SetTargetPlayer(UpdatedPlayerList[iMinHealthPlayerIndex]);
  }
}
