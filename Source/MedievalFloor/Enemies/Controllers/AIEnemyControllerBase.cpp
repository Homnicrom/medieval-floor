// Fill out your copyright notice in the Description page of Project Settings.

#include "AIEnemyControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "../../Characters/Base/CharacterBase.h"
#include "../../Characters/PlayerCharacter.h"
#include "../EnemyClass/EnemiesBase.h"
#include "../Library/GameTeamsUtilities.h"
#include "MedievalFloor/GameMode/GameplayGameMode.h"
#include "Engine/World.h"

AAIEnemyControllerBase::AAIEnemyControllerBase()
{
  PrimaryActorTick.bCanEverTick = true;

  // Setear el equipo para que los enemigos se ignoren entre si y solo detecten a los jugadores.
  teamId = FGenericTeamId(UGameTeamsUtilities::EnemyTeam);

  targetPlayerBBReference = TEXT("DetectedPlayer");
  lastKnownPositionBBReference = TEXT("LastKnownPosition");
  
  // Inicializar componentes (DEPRECATED)
  //pAIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
  //pSightPerceptionComponent = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightPerception"));
  //pSightPerceptionComponent->SightRadius = 1500.f;
  //pSightPerceptionComponent->LoseSightRadius = 1700.f;
  //pSightPerceptionComponent->PeripheralVisionAngleDegrees = 180.f;
  //pSightPerceptionComponent->DetectionByAffiliation.bDetectEnemies = true;
  //pSightPerceptionComponent->DetectionByAffiliation.bDetectNeutrals = true;
  //pSightPerceptionComponent->DetectionByAffiliation.bDetectFriendlies = true;
  //pAIPerceptionComponent->ConfigureSense(*pSightPerceptionComponent);
  //pAIPerceptionComponent->SetDominantSense(pSightPerceptionComponent->GetSenseImplementation());
  // -----------------------------------------------------------------------------------------------

  // El controlador attached con el pawn
  bAttachToPawn = true;
}

void AAIEnemyControllerBase::BeginPlay()
{
  Super::BeginPlay();

  //InitStatsFromDataTable();
}

void AAIEnemyControllerBase::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
}

void AAIEnemyControllerBase::OnPossess(APawn* _pPawn)
{
  Super::OnPossess(_pPawn);

  // DEPRECATED
  //pAIPerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AAIEnemyControllerBase::DefaultSightSenseUpdate);
  //OnSightUpdated.Broadcast(GetDetectedPlayers());

  // Run behaviour tree
  ensure(pBehaviorTreeAsset != nullptr);
  if (pBehaviorTreeAsset != nullptr)
  {
    RunBehaviorTree(pBehaviorTreeAsset);
  }
  ChoosePlayer();
}

ETeamAttitude::Type AAIEnemyControllerBase::GetTeamAttitudeTowards(const AActor& Other) const
{
  const IGenericTeamAgentInterface* pTeamAgent = Cast<IGenericTeamAgentInterface>(&Other);
  if (pTeamAgent != nullptr)
  {
    uint8 myTeam = uint8(GetGenericTeamId());
    uint8 otherTeam = uint8(pTeamAgent->GetGenericTeamId());
    return myTeam == otherTeam ? ETeamAttitude::Friendly : ETeamAttitude::Hostile;
  }
  // By default
  return ETeamAttitude::Neutral;
}


const TArray<APlayerCharacter*>& AAIEnemyControllerBase::GetDetectedPlayers() const
{
  AGameplayGameMode* gameMode = GetWorld()->GetAuthGameMode<AGameplayGameMode>();
  //ensure(gameMode != nullptr);
  if (gameMode == nullptr)
  {
    return detectedPlayersArray;
  }
  return gameMode->PlayerList;
}

void AAIEnemyControllerBase::SetTargetPlayer(APlayerCharacter* player)
{
  ensure(!targetPlayerBBReference.IsNone());
  UObject* playerObject = Cast<UObject>(player);
  ensure(playerObject != nullptr);
  GetBlackboardComponent()->SetValueAsObject(targetPlayerBBReference, playerObject);
}

APlayerCharacter* AAIEnemyControllerBase::GetTargetPlayer() const
{
  ensure(!targetPlayerBBReference.IsNone());
  return Cast<APlayerCharacter>(GetBlackboardComponent()->GetValueAsObject(targetPlayerBBReference));
}

void AAIEnemyControllerBase::ClearTargetPlayer()
{
  APlayerCharacter* player = GetTargetPlayer();
  if (player == nullptr)
  {
    return;
  }
  player->onKillPlayerDelegate.RemoveDynamic(this, &AAIEnemyControllerBase::ChoosePlayer);
  GetBlackboardComponent()->ClearValue(targetPlayerBBReference);
}

void AAIEnemyControllerBase::DefaultSightSenseUpdate(const TArray<AActor*>& UpdatedActors)
{
  UpdatePlayerArray(UpdatedActors);

  if (targetPlayerBBReference.IsNone())
  {
    UE_LOG(LogTemp, Warning, TEXT("[%s(%d)] Target player blackboard key is none."), *FString(__FILE__), __LINE__);
    return;
  }

  UBlackboardComponent* pBlackboardComponent = GetBlackboardComponent();
  ensure(pBlackboardComponent != nullptr);
  APlayerCharacter* detectedPlayer = Cast<APlayerCharacter>(pBlackboardComponent->GetValueAsObject(targetPlayerBBReference));

  if (detectedPlayersArray.Num() != 0) // There are players in sight
  {
    if (detectedPlayer == nullptr || detectedPlayersArray.Find(detectedPlayer) == INDEX_NONE)
    {
      OnSightUpdated.Broadcast(detectedPlayersArray);
    }
  }
  else
  {
    if (detectedPlayer != nullptr)
    {
      pBlackboardComponent->SetValueAsVector(lastKnownPositionBBReference, detectedPlayer->GetActorLocation());
      pBlackboardComponent->ClearValue(targetPlayerBBReference);
    }
  }
}

void AAIEnemyControllerBase::InitStatsFromDataTable()
{
  // DEPRECATED
  //ensure(pEnemyControllerStatsTable != nullptr);
  //if (pEnemyControllerStatsTable != nullptr)
  //{
  //  FEnemyControllerStats* pEnemyStats = pEnemyControllerStatsTable->FindRow<FEnemyControllerStats>(FEnemyType::AsName(enemyTypeSelector.GetValue()), "");
  //  ensure(pEnemyStats != nullptr);
  //  if (pEnemyStats != nullptr)
  //  {
  //    fAttackProbability = pEnemyStats->attackProbability;
  //    // DEPRECATED
  //    /*fChaseSpeed = pEnemyStats->chaseSpeed;
  //    fPatrolSpeed = pEnemyStats->patrolSpeed;*/
  //  }
  //}
}

void AAIEnemyControllerBase::UpdatePlayerArray(const TArray<AActor*>& UpdatedActors)
{
  AEnemiesBase* controlledEnemy = Cast<AEnemiesBase>(GetPawn());
  ensure(controlledEnemy != nullptr);
  for (auto detectedActor : UpdatedActors)
  {
    if (!detectedActor->ActorHasTag(controlledEnemy->GetEnemyTag()))
    {
      APlayerCharacter* pPlayer = Cast<APlayerCharacter>(detectedActor);
      if (pPlayer != nullptr)
      {
        int32 iIndex = detectedPlayersArray.Find(pPlayer);
        if (iIndex == INDEX_NONE)
        {
          detectedPlayersArray.Add(pPlayer);
          UE_LOG(LogTemp, Warning, TEXT("%s: Adding player to detected players list: %s"), *GetName(), *pPlayer->GetName());
        }
        else
        {
          detectedPlayersArray.RemoveAt(iIndex);
          UE_LOG(LogTemp, Warning, TEXT("%s: Removing player to detected players list: %s"), *GetName(), *pPlayer->GetName());
        }
      }
    }
  }
}

TArray<APlayerCharacter*> AAIEnemyControllerBase::GetFilteredPlayerList(const TArray<APlayerCharacter*>& UpdatedPlayerList)
{
  TArray<APlayerCharacter*> playerList;
  for (APlayerCharacter* player : UpdatedPlayerList)
  {
    if (player->GetPlayerCurrentHealth() > 0)
    {
      playerList.Add(player);
    }
  }
  return playerList;
}

void AAIEnemyControllerBase::ChoosePlayer()
{}

void AAIEnemyControllerBase::UpdateTargetPlayer()
{
  ChoosePlayer();
}

