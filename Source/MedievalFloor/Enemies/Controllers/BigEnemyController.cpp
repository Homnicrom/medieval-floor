// Fill out your copyright notice in the Description page of Project Settings.


#include "BigEnemyController.h"
#include "../../Characters/Base/CharacterBase.h"
#include "../../Characters/PlayerCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"

ABigEnemyController::ABigEnemyController()
{
  PrimaryActorTick.bCanEverTick = true;
  // DEPRECATED
  //OnSightUpdated.AddDynamic(this, &ABigEnemyController::SightSenseUpdate);

}

void ABigEnemyController::BeginPlay()
{
  Super::BeginPlay();
  // ...
}

void ABigEnemyController::SightSenseUpdate(const TArray<APlayerCharacter*>& UpdatedPlayerList)
{
  ChoosePlayer_Internal(UpdatedPlayerList);
}

void ABigEnemyController::ChoosePlayer()
{
  ChoosePlayer_Internal(GetDetectedPlayers());
}

void ABigEnemyController::ChoosePlayer_Internal(const TArray<APlayerCharacter*>& UpdatedPlayerList)
{
  if (UpdatedPlayerList.Num() != 0)
  {
    ClearTargetPlayer();
    int32 iMinDistancePlayerIndex = 0;
    float fMinDistance = 0.f;
    APawn* controlledPawn = GetPawn();
    ensure(controlledPawn != nullptr);
    for (int32 iIndex = 0; iIndex < UpdatedPlayerList.Num(); ++iIndex)
    {
      if (UpdatedPlayerList[iIndex]->GetPlayerCurrentHealth() > 0)
      {
        float fDistance = (UpdatedPlayerList[iIndex]->GetActorLocation() - controlledPawn->GetActorLocation()).Size();
        if (fDistance < fMinDistance)
        {
          iMinDistancePlayerIndex = iIndex;
          fMinDistance = fDistance;
        }
      }
    }

    // Bind player kill event
    UpdatedPlayerList[iMinDistancePlayerIndex]->onKillPlayerDelegate.AddDynamic(this, &AAIEnemyControllerBase::ChoosePlayer);
    SetTargetPlayer(UpdatedPlayerList[iMinDistancePlayerIndex]);
  }
}
