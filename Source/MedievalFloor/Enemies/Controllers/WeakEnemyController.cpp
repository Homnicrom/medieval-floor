// Fill out your copyright notice in the Description page of Project Settings.


#include "WeakEnemyController.h"
#include "../../Characters/Base/CharacterBase.h"
#include "../../Characters/PlayerCharacter.h"
#include "Perception/AIPerceptionComponent.h"
#include "Math/UnrealMathUtility.h"
#include "BehaviorTree/BlackboardComponent.h"


AWeakEnemyController::AWeakEnemyController()
{
  PrimaryActorTick.bCanEverTick = true;
  // DEPRECATED
  //OnSightUpdated.AddDynamic(this, &AWeakEnemyController::SightSenseUpdate);
}

void AWeakEnemyController::BeginPlay()
{
  Super::BeginPlay();
}

//void AWeakEnemyController::SightSenseUpdate(const TArray<APlayerCharacter*>& UpdatedPlayerList)
//{
//  ChoosePlayer_Internal(UpdatedPlayerList);
//}

void AWeakEnemyController::ChoosePlayer()
{
  TArray<APlayerCharacter*> playerList = GetFilteredPlayerList(GetDetectedPlayers());
  ChoosePlayer_Internal(playerList);
}

void AWeakEnemyController::ChoosePlayer_Internal(const TArray<APlayerCharacter*>& UpdatedPlayerList)
{
  if (UpdatedPlayerList.Num() != 0)
  {
    ClearTargetPlayer();
    int32 iRandomPlayerIndex = FMath::RandRange(0, UpdatedPlayerList.Num() - 1);
    UpdatedPlayerList[iRandomPlayerIndex]->onKillPlayerDelegate.AddDynamic(this, &AAIEnemyControllerBase::ChoosePlayer);
    SetTargetPlayer(UpdatedPlayerList[iRandomPlayerIndex]);
  }
}
