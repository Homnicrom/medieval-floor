// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AIEnemyControllerBase.h"
#include "BehaviorTree/BehaviorTreeTypes.h"
#include "WeakEnemyController.generated.h"

class ACharacterBase;
class APlayerCharacter;

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API AWeakEnemyController : public AAIEnemyControllerBase
{
	GENERATED_BODY()

public:

	// Sets default values for controller's properties
	AWeakEnemyController();

protected:

  virtual void BeginPlay() override;

public:
  /**
  * DEPRECATED
  * @brief Evento de actualizacion del campo de vision
  */
  //UFUNCTION(Category = "AI Perception|Sight")
  //  void SightSenseUpdate(const TArray<APlayerCharacter*>& UpdatedPlayerList);

  /**
  * @brief Override del controlador base para la eleccion del jugador objetivo
  */
  UFUNCTION()
  virtual void ChoosePlayer() override;

private:
  /**
  * @brief Logica de eleccion de jugador objetivo
  */
  void ChoosePlayer_Internal(const TArray<APlayerCharacter*>& UpdatedPlayerList);
};
