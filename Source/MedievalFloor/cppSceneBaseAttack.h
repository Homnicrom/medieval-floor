// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "cppSceneBaseAttack.generated.h"


UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MEDIEVALFLOOR_API UcppSceneBaseAttack : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UcppSceneBaseAttack();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UPROPERTY(BlueprintReadWrite)
	bool isCharging = false;
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void StartCharge();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Release();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void FinishAttack();
		
};
