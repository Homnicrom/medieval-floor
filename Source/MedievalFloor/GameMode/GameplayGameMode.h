// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/GameModeBase.h"
#include "MedievalFloor/OtherElements/ControllerUtil.h"
#include "MedievalFloor/Characters/PlayerCharacter.h"
#include "MedievalFloor/Enemies/EnemyClass/EnemiesBase.h"
#include "MedievalFloor/Enemies/Extra/EnemyDifficultyStruct.h"
#include "MedievalFloor/Managers/WaveManager.h"

#include "GameplayGameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPlayerReadyEventSignature, uint8, _playerID, bool, _readyStatus);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameplayReadyEventSignature);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGameOverEventSignature, int, _score, bool, _isHighScore);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGamePausedEventSignature, bool, _paused);

UCLASS()
class MEDIEVALFLOOR_API AGameplayGameMode : public AGameModeBase
{
  GENERATED_BODY()

  AGameplayGameMode();
  ~AGameplayGameMode();

  virtual void BeginPlay() override;
  virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
  /*
  * DELEGATES
  */

  /**
   * @brief Cada vez que un player pulse A/B (xbox) o X/O (ps4) en un mando, se enviar� un mensaje con el ID del player (1-4) y su estado (true -> ha pulsado A; false -> ha pulsado B)
   */
  UPROPERTY(BlueprintAssignable)
  FPlayerReadyEventSignature PlayerReadyEventDelegate;

  /**
   * @brief Cuando todos los peronajes est�n ready, uno de ellos puede pulsar el MENU button (pause) para confirmar y empezar el gameplay. Este evento se lanzar� DESPU�S de haber spawneado los pawns del gameplay
   */
  UPROPERTY(BlueprintAssignable)
  FGameplayReadyEventSignature GameplayReadyEventDelegate;

  UPROPERTY(BlueprintAssignable)
  FGameOverEventSignature GameOverEventDelegate;

  UPROPERTY(BlueprintAssignable)
  FGamePausedEventSignature GamePausedEventDelegate;

  /*
   * PROPERTIES
   */

  /**
   * @brief Cantidad de jugadores (debe ser > 1)
   */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameSetup)
  uint8 NumberOfPlayers = 4;

  /**
   * @brief Pawns que se usar�n para los personajes
   */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameSetup)
  TArray<TSubclassOf<APlayerCharacter>> PlayerPawnsToSpawn;

  /**
   * @brief Lista de los personjes (pawns) durante el gameplay. Accesible si se desea ver el valor de la vida u otras propiedades de alg�n personaje concreto.
   */
  UPROPERTY()
  TArray<APlayerCharacter*> PlayerList;

  /**
   * @brief Pawn temporal usado para la selecci�n de mandos
  */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameSetup)
  TSubclassOf<APlayerCharacter> PreviewPawn;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemiesAndDifficulty)
  TArray<FEnemyDifficultyStruct> EnemiesList;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemiesAndDifficulty)
  uint8 StartingDifficultyLevel = 10;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemiesAndDifficulty)
  float TimeBetweenWaves = 5;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemiesAndDifficulty)
  float TimeBetweenEachSpawn = 0.5f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemiesAndDifficulty)
  uint8 DifficultyIncreaseBetweenWaves = 5;

  /**
   * @brief Lista de UControllerUtils (con PlayerControllers dentro) ordenados
   */
  UPROPERTY()
  TArray<UControllerUtil*> ControllersList;

  UPROPERTY()
  UWaveManager* WaveManager = nullptr;

private:
  /**
  * @brief Puntos de spawn iniciales para los personajes
  */
  UPROPERTY()
  TArray<AActor*> PlayerSpawnPoints;

  int* iPlayersOrderArray = nullptr;
  int iNumOfPlayersAssigned = 0;
  int iNumPlayersDead = 0;

  void RecreateControllersWithAssignedOrder();
  void UnbindAllControllerActions();

  bool CheckIfAllPlayersAreAssigned() const;

  void RemoveAllPlayerControllers() const;
  void SpawnGameplayPawns();
  void SpawnPreviewPawns();

  /**
   * @brief Lista para los pawns que se usan durante la confirmaci�n de los mandos
   */
  TArray<APlayerCharacter*> previewPawnsArray;

  /*
   * InputAction event keys
   */
  UFUNCTION()
  void ConfirmButtonPressed(int _id);
  UFUNCTION()
  void CancelButtonPressed(int _id);
  UFUNCTION()
  void EndControllersBindingButtonPressed();
  UFUNCTION()
  void PauseButtonPressed();

  /*
   * InputAction helpers
   */
  void AssignControllerToPlayer(int _id);
  void UnassignControllerToPlayer(int _id);

  /*
   * Other events
   */
  UFUNCTION()
  void CharacterHealthChanged(APlayerCharacter* _pDamagedCharacter);
  UFUNCTION()
  void ChangeLevel();

  /*
   * SINGLEPLAYER
   * HACK
   * AREA
   */
  FInputActionBinding hackButtons[5];

  UFUNCTION()
  void HackButtonPressed(int _id);
};
