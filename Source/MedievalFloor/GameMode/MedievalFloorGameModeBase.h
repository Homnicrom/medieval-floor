// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MedievalFloorGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API AMedievalFloorGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
