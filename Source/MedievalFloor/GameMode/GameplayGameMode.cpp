// Fill out your copyright notice in the Description page of Project Settings.

#include "GameplayGameMode.h"

#include "Kismet/GameplayStatics.h"
#include "MedievalFloor/GameInstance/MedievalFloorGameInstance.h"
#include "MedievalFloor/Managers/LevelManager.h"
#include "MedievalFloor/OtherElements/Spawner.h"

AGameplayGameMode::AGameplayGameMode()
{
  DefaultPawnClass = nullptr;
}

AGameplayGameMode::~AGameplayGameMode()
{
}

void AGameplayGameMode::BeginPlay()
{
  Super::BeginPlay();

  ensure(NumberOfPlayers > 1);

  ensure(EnemiesList.Num() > 0);
  ensure(StartingDifficultyLevel > 0);
  ensure(TimeBetweenWaves > 0);
  ensure(DifficultyIncreaseBetweenWaves > 0);

  iPlayersOrderArray = new int[NumberOfPlayers];
  for (unsigned int i = 0; i < NumberOfPlayers; ++i)
  {
    iPlayersOrderArray[i] = -1;
  }

  //Obtener todos los spawners de players
  UGameplayStatics::GetAllActorsOfClassWithTag(this, ASpawner::StaticClass(), TEXT("PlayerSpawner"), PlayerSpawnPoints);

  //Obtener todos los spawners enemigos
  TArray<AActor*> EnemySpawnPoints;
  UGameplayStatics::GetAllActorsOfClassWithTag(this, ASpawner::StaticClass(), TEXT("EnemySpawner"), EnemySpawnPoints);

  //Inicializar el wave manager
  WaveManager = NewObject<UWaveManager>(this);
  WaveManager->Init(EnemiesList, EnemySpawnPoints, TimeBetweenWaves, TimeBetweenEachSpawn, DifficultyIncreaseBetweenWaves, StartingDifficultyLevel);
  EnemiesList.Reset();
  EnemySpawnPoints.Reset();

  //Eliminar todos los player controllers que pueda haber (UE crea uno por defecto)
  RemoveAllPlayerControllers();

  //Crear nuevos player controllers y bindearles las acciones para la selecci�n de mandos
  UWorld* pWorld = GetWorld();
  for (unsigned int i = 0; i < NumberOfPlayers; i++)
  {
    UControllerUtil* pController = NewObject<UControllerUtil>(this);

    const FInputActionBinding confirmBinding = UControllerUtil::CreateOneParamBinding(this, i, FName("ConfirmButton"), FName("ConfirmButtonPressed"));
    const FInputActionBinding backBinding = UControllerUtil::CreateOneParamBinding(this, i, FName("BackButton"), FName("CancelButtonPressed"));
    const FInputActionBinding pauseBinding = UControllerUtil::CreateOneParamBinding(this, i, FName("PauseButton"), FName("EndControllersBindingButtonPressed"));

    pController->PlayerController = UGameplayStatics::CreatePlayer(pWorld, i, true);
    pController->ConfirmBinding = confirmBinding;
    pController->BackBinding = backBinding;
    pController->PauseBinding = pauseBinding;

    pController->PlayerController->InputComponent->AddActionBinding(confirmBinding);
    pController->PlayerController->InputComponent->AddActionBinding(backBinding);
    pController->PlayerController->InputComponent->AddActionBinding(pauseBinding);

    ControllersList.Add(pController);
  }

  SpawnPreviewPawns();

  /*
   * SINGLEPLAYER
   * HACK
   * AREA
   */
  hackButtons[0] = UControllerUtil::CreateOneParamBinding(this, 1, FName("HackSelectionBtn1"), FName("HackButtonPressed"));
  hackButtons[1] = UControllerUtil::CreateOneParamBinding(this, 2, FName("HackSelectionBtn2"), FName("HackButtonPressed"));
  hackButtons[2] = UControllerUtil::CreateOneParamBinding(this, 3, FName("HackSelectionBtn3"), FName("HackButtonPressed"));
  hackButtons[3] = UControllerUtil::CreateOneParamBinding(this, 4, FName("HackSelectionBtn4"), FName("HackButtonPressed"));
  hackButtons[4] = UControllerUtil::CreateOneParamBinding(this, 5, FName("HackSelectionBtn5"), FName("HackButtonPressed"));

  for (unsigned int i = 0; i < NumberOfPlayers; i++)
  {
    ControllersList[i]->PlayerController->InputComponent->AddActionBinding(hackButtons[0]);
    ControllersList[i]->PlayerController->InputComponent->AddActionBinding(hackButtons[1]);
    ControllersList[i]->PlayerController->InputComponent->AddActionBinding(hackButtons[2]);
    ControllersList[i]->PlayerController->InputComponent->AddActionBinding(hackButtons[3]);
    ControllersList[i]->PlayerController->InputComponent->AddActionBinding(hackButtons[4]);
  }
}

void AGameplayGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
  Super::EndPlay(EndPlayReason);

  WaveManager = nullptr;

  ControllersList.Reset();
  PlayerList.Reset();

  PlayerReadyEventDelegate.RemoveAll(GetWorld());
  GameplayReadyEventDelegate.RemoveAll(GetWorld());
  GameOverEventDelegate.RemoveAll(GetWorld());
  GamePausedEventDelegate.RemoveAll(GetWorld());

  delete[] iPlayersOrderArray;
}

void AGameplayGameMode::ConfirmButtonPressed(int _id)
{
  AssignControllerToPlayer(_id);
}

void AGameplayGameMode::CancelButtonPressed(int _id)
{
  UnassignControllerToPlayer(_id);
}

void AGameplayGameMode::EndControllersBindingButtonPressed()
{
  UE_LOG(LogTemp, Warning, TEXT("EndControllersBinding button pressed."));

  ensure(NumberOfPlayers > 1);

  //si todos los players est�n seteados
  if (CheckIfAllPlayersAreAssigned())
  {
    for (int i = 0; i < previewPawnsArray.Num(); ++i)
    {
      ControllersList[i]->PlayerController->UnPossess();
      previewPawnsArray[i]->Destroy();
    }
    previewPawnsArray.Reset();

    UnbindAllControllerActions();

    RecreateControllersWithAssignedOrder();

    SpawnGameplayPawns();

    //Evento de que el gameplay est� ready
    GameplayReadyEventDelegate.Broadcast();

    WaveManager->Start();

    TArray<AActor*> outActors;
    UGameplayStatics::GetAllActorsWithTag(this, FName("Manager"), outActors);
    ULevelManager* pLevelManager = Cast<ULevelManager>(outActors[0]->GetComponentByClass(ULevelManager::StaticClass()));
    pLevelManager->OnChangeLevelEventDelegate.AddDynamic(this, &AGameplayGameMode::ChangeLevel);
  }
}

void AGameplayGameMode::PauseButtonPressed()
{
  UE_LOG(LogTemp, Warning, TEXT("GAME PAUSED"));
  UWorld* pWorld = GetWorld();
  GamePausedEventDelegate.Broadcast(!UGameplayStatics::IsGamePaused(pWorld));
  UGameplayStatics::SetGamePaused(pWorld, !UGameplayStatics::IsGamePaused(pWorld));
}

void AGameplayGameMode::AssignControllerToPlayer(int _id)
{
  ensure(NumberOfPlayers > 1);

  int iFirstEmptySlot = -1;
  for (unsigned int i = 0; i < NumberOfPlayers; i++)
  {
    if (iPlayersOrderArray[i] == -1 && iFirstEmptySlot == -1)
    {
      iFirstEmptySlot = i; //guardamos el primer slot libre para luego asignarlo.
    }
    else
    {
      if (iPlayersOrderArray[i] == _id)
      {
        return; //si el ID ya est� en el array, paramos y no hacemos nada.
      }
    }
  }

  //si no se encuentra un emptySlot es que ya no hay nada que asignar, por lo que no se har� nada
  if (iFirstEmptySlot != -1)
  {
    iPlayersOrderArray[iFirstEmptySlot] = _id;
    UE_LOG(LogTemp, Warning, TEXT("CONTROLLER %d assigned to PLAYER %d."), _id, iFirstEmptySlot + 1);
    UE_LOG(LogTemp, Warning, TEXT("PLAYER %d READY."), iFirstEmptySlot + 1);

    PlayerReadyEventDelegate.Broadcast(iFirstEmptySlot, true);
    iNumOfPlayersAssigned++;
  }
}

void AGameplayGameMode::UnassignControllerToPlayer(int _id)
{
  ensure(NumberOfPlayers > 1);

  int iRemovedSlot = -1;
  for (unsigned int i = 0; i < NumberOfPlayers; i++)
  {
    if (iPlayersOrderArray[i] == _id)
    {
      iPlayersOrderArray[i] = -1; //si encontramos el controller en la lista, lo quitamos
      iRemovedSlot = i;
      break;
    }
  }
  if (iRemovedSlot != -1)
  {
    UE_LOG(LogTemp, Warning, TEXT("CONTROLLER %d unassigned."), _id);
    UE_LOG(LogTemp, Warning, TEXT("PLAYER %d NOT READY."), iRemovedSlot + 1);
    PlayerReadyEventDelegate.Broadcast(iRemovedSlot, false);
    iNumOfPlayersAssigned--;
  }
}

void AGameplayGameMode::CharacterHealthChanged(APlayerCharacter* _pDamagedCharacter)
{
  if (_pDamagedCharacter && _pDamagedCharacter->GetPlayerCurrentHealth() == 0)
  {
    _pDamagedCharacter->healthChangedEvent.RemoveDynamic(this, &AGameplayGameMode::CharacterHealthChanged);
    iNumPlayersDead++;
  }

  if (iNumPlayersDead == NumberOfPlayers)
  {
    WaveManager->Stop();

    const int iScore = WaveManager->GetScore();;
    UMedievalFloorGameInstance* pGameInstance = Cast<UMedievalFloorGameInstance>(UGameplayStatics::GetGameInstance(this));
    const bool bHighScore = pGameInstance->NewScore(iScore);

    UnbindAllControllerActions();

    GameOverEventDelegate.Broadcast(iScore, bHighScore);
  }
}

void AGameplayGameMode::ChangeLevel()
{
  WaveManager->Stop();
  RemoveAllPlayerControllers();

  //A�adir un controller nuevo por defecto
  UGameplayStatics::CreatePlayer(this, 0, true);

  TArray<AActor*> outActors;
  UGameplayStatics::GetAllActorsWithTag(this, FName("Manager"), outActors);
  ULevelManager* pLevelManager = Cast<ULevelManager>(outActors[0]->GetComponentByClass(ULevelManager::StaticClass()));
  pLevelManager->OnChangeLevelEventDelegate.RemoveDynamic(this, &AGameplayGameMode::ChangeLevel);
}

void AGameplayGameMode::HackButtonPressed(int _id)
{
  iNumOfPlayersAssigned = 0;
  if (_id != 5)
  {
    for (unsigned int i = 0; i < NumberOfPlayers; i++)
    {
      if (iPlayersOrderArray[i] != -1)
      {
        iPlayersOrderArray[i] = -1;
        PlayerReadyEventDelegate.Broadcast(i, false);
      }
      if (i != _id - 1)
      {
        iPlayersOrderArray[i] = 5 + i; //no asignar a nadie
        iNumOfPlayersAssigned++;
        PlayerReadyEventDelegate.Broadcast(i, true);
      }
    }
  }
  else //eliminar todas las asignaciones
  {
    for (unsigned int i = 0; i < NumberOfPlayers; i++)
    {
      if (iPlayersOrderArray[i] != -1)
      {
        iPlayersOrderArray[i] = -1;
        PlayerReadyEventDelegate.Broadcast(i, false);
      }
    }
  }
}

void AGameplayGameMode::RecreateControllersWithAssignedOrder()
{
  UWorld* pWorld = GetWorld();
  for (unsigned int i = 0; i < NumberOfPlayers; i++)
  {
    UGameplayStatics::RemovePlayer(ControllersList[i]->PlayerController, true);
  }

  ControllersList.Reset();

  for (unsigned int i = 0; i < NumberOfPlayers; i++)
  {
    UControllerUtil* pController = NewObject<UControllerUtil>(this);

    FInputActionBinding pauseBinding = UControllerUtil::CreateOneParamBinding(this, i + 1, FName("PauseButton"), FName("PauseButtonPressed"));
    pauseBinding.bExecuteWhenPaused = true;

    pController->PlayerController = UGameplayStatics::CreatePlayer(pWorld, iPlayersOrderArray[i], true);
    pController->PauseBinding = pauseBinding;

    pController->PlayerController->InputComponent->AddActionBinding(pauseBinding);

    ControllersList.Add(pController);
  }
}

void AGameplayGameMode::UnbindAllControllerActions()
{
  for (unsigned int i = 0; i < NumberOfPlayers; i++)
  {
    ControllersList[i]->PlayerController->InputComponent->ClearActionBindings();
  }
}

bool AGameplayGameMode::CheckIfAllPlayersAreAssigned() const
{
  ensure(NumberOfPlayers > 1);

  return iNumOfPlayersAssigned == NumberOfPlayers;
}

void AGameplayGameMode::RemoveAllPlayerControllers() const
{
  TArray<AActor*> PCList;
  UGameplayStatics::GetAllActorsOfClass(this, APlayerController::StaticClass(), PCList);
  for (int i = 0; i < PCList.Num(); i++)
  {
    APlayerController* PC = Cast<APlayerController>(PCList[i]);
    UGameplayStatics::RemovePlayer(PC, true);
  }
}

void AGameplayGameMode::SpawnPreviewPawns()
{
  ensure(NumberOfPlayers > 1);
  ensure(PlayerSpawnPoints.Num() >= NumberOfPlayers);
  ensure(ControllersList.Num() == NumberOfPlayers);
  ensure(PreviewPawn != nullptr);

  UWorld* pWorld = GetWorld();
  FActorSpawnParameters spawnParameters;
  for (unsigned int i = 0; i < NumberOfPlayers; i++)
  {
    spawnParameters.Name = *FString::Printf(TEXT("PreviewPlayer_%d"), i + 1);
    spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

    APlayerCharacter* pSpawnedPawn = pWorld->SpawnActor<APlayerCharacter>(PreviewPawn, PlayerSpawnPoints[i]->GetTransform(), spawnParameters);
    ControllersList[i]->PlayerController->Possess(pSpawnedPawn);
    previewPawnsArray.Add(pSpawnedPawn);
  }
}

void AGameplayGameMode::SpawnGameplayPawns()
{
  ensure(NumberOfPlayers > 1);
  ensure(PlayerSpawnPoints.Num() >= NumberOfPlayers);
  ensure(PlayerPawnsToSpawn.Num() >= NumberOfPlayers);
  ensure(ControllersList.Num() == NumberOfPlayers);

  UWorld* pWorld = GetWorld();
  FActorSpawnParameters spawnParameters;
  spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
  for (unsigned int i = 0; i < NumberOfPlayers; i++)
  {
    spawnParameters.Name = *FString::Printf(TEXT("Player_%d"), i + 1);

    APlayerCharacter* pSpawnedPawn = pWorld->SpawnActor<APlayerCharacter>(PlayerPawnsToSpawn[i], PlayerSpawnPoints[i]->GetTransform(), spawnParameters);
    pSpawnedPawn->healthChangedEvent.AddDynamic(this, &AGameplayGameMode::CharacterHealthChanged);
    ControllersList[i]->PlayerController->Possess(pSpawnedPawn);
    PlayerList.Add(pSpawnedPawn);
  }
}
