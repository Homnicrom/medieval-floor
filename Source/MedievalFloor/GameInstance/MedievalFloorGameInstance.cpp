// Fill out your copyright notice in the Description page of Project Settings.

#include "MedievalFloorGameInstance.h"

#include "Kismet/GameplayStatics.h"

UMedievalFloorGameInstance::UMedievalFloorGameInstance()
{
  CreateSavedGame();
  LoadGame();
}

bool UMedievalFloorGameInstance::NewScore(int _score)
{
  if (_score > iHighScore)
  {
    iHighScore = _score;
    bDirtyScore = true;

    SaveGame();

    return true;
  }
  return false;
}

void UMedievalFloorGameInstance::LoadGame()
{
  if (bDirtyScore)
  {
    if (UGameplayStatics::DoesSaveGameExist(CurrentSaveGame->SaveSlot, CurrentSaveGame->SaveUserIndex))
    {
      CurrentSaveGame = Cast<UScoreSaveGame>(UGameplayStatics::LoadGameFromSlot(CurrentSaveGame->SaveSlot, CurrentSaveGame->SaveUserIndex));
      iHighScore = CurrentSaveGame->Score;
    }
  }
  bDirtyScore = false;
}

void UMedievalFloorGameInstance::SaveGame()
{
  if (bDirtyScore)
  {
    CurrentSaveGame->Score = iHighScore;
    UGameplayStatics::SaveGameToSlot(CurrentSaveGame, CurrentSaveGame->SaveSlot, CurrentSaveGame->SaveUserIndex);
    bDirtyScore = false;
  }
}

int UMedievalFloorGameInstance::GetHighScore() const
{
  return iHighScore;
}

bool UMedievalFloorGameInstance::CreateSavedGame()
{
  CurrentSaveGame = Cast<UScoreSaveGame>(UGameplayStatics::CreateSaveGameObject(UScoreSaveGame::StaticClass()));
  return CurrentSaveGame != nullptr;
}
