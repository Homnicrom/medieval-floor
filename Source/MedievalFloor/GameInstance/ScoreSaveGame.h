// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "ScoreSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UScoreSaveGame : public USaveGame
{
  GENERATED_BODY()
public:
  UScoreSaveGame();

  UPROPERTY(BlueprintReadWrite, Category = Save)
  FString SaveSlot;

  UPROPERTY(BlueprintReadWrite, Category = Save)
  int32 SaveUserIndex;

  UPROPERTY(BlueprintReadOnly, Category = Save)
  int32 Score;
};
