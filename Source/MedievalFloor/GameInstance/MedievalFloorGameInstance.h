// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ScoreSaveGame.h"
#include "MedievalFloorGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UMedievalFloorGameInstance : public UGameInstance
{
  GENERATED_BODY()

  int iHighScore = 0;
  bool bDirtyScore = true;

  UPROPERTY()
  UScoreSaveGame* CurrentSaveGame;

public:
  UMedievalFloorGameInstance();

  UFUNCTION(BlueprintCallable, Category = Save)
  bool NewScore(int _score);

  UFUNCTION(BlueprintCallable, Category = Save)
  void LoadGame();

  UFUNCTION(BlueprintCallable, Category = Save)
  void SaveGame();

  int GetHighScore() const;

protected:
  UFUNCTION(BlueprintCallable, Category = Save)
  bool CreateSavedGame();
};
