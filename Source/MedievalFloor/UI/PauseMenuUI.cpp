// Fill out your copyright notice in the Description page of Project Settings.


#include "PauseMenuUI.h"
#include "Kismet/GameplayStatics.h"

void UPauseMenuUI::NativeConstruct()
{
	Super::NativeConstruct();

	TArray<AActor*> outActors;
	UGameplayStatics::GetAllActorsWithTag(this, FName("Manager"), outActors);

	pLevelManager = Cast<ULevelManager>(outActors[0]->GetComponentByClass(ULevelManager::StaticClass()));
	pUIManager = Cast<UUIManager>(outActors[0]->GetComponentByClass(UUIManager::StaticClass()));

	FText pauseMenuText = FText::FromString("Pause Menu");
	FText resumeButtonText = FText::FromString("Resume");
	FText exitButtonText = FText::FromString("Exit to Main Menu");

	pPauseMenu_Text->SetText(pauseMenuText);
	pResumeButton_Text->SetText(resumeButtonText);
	pExitButton_Text->SetText(exitButtonText);

	pResumeButton->OnClicked.AddDynamic(this, &UPauseMenuUI::ResumeGame);
	pExitButton->OnClicked.AddDynamic(this, &UPauseMenuUI::ExitToMainMenu);
}

void UPauseMenuUI::ResumeGame()
{
  UWorld* pWorld = GetWorld();
  UGameplayStatics::SetGamePaused(pWorld, !UGameplayStatics::IsGamePaused(pWorld));
	pUIManager->LoadPauseUI(false);
}

void UPauseMenuUI::ExitToMainMenu()
{
	//Es c�clico as� que nos llevar� al men� principal.[Revisar Animacion]
	pLevelManager->LoadLevel();
}