// Fill out your copyright notice in the Description page of Project Settings.


#include "GlobalGameUI.h"

void UGlobalGameUI::NativeConstruct()
{
  Super::NativeConstruct();

  FText initialScoreText = FText::FromString("Score:  0");
  FText initialWaveText = FText::FromString("Wave:  1");
  //FText initialReadyPlayersText = FText::FromString("Press options/menu to start");

  pWaveNumberText->SetText(initialWaveText);
  pScoreText->SetText(initialScoreText);
  //pReadyPlayersText->SetText(initialReadyPlayersText);

  pWaveNumberText->SetVisibility(ESlateVisibility::Hidden);
  pScoreText->SetVisibility(ESlateVisibility::Hidden);
  //pReadyPlayersText->SetVisibility(ESlateVisibility::Hidden);
  pReadyCanvasBox->SetVisibility(ESlateVisibility::Hidden);
  pGameplayUIHorizontalBox->SetVisibility(ESlateVisibility::Hidden);
  //pReadyPlayersHorizontalBox->SetVisibility(ESlateVisibility::Hidden);
}

void UGlobalGameUI::UpdateWaveText(int _iWave)
{
  FText newWaveText = FText::FromString("Wave:  " + FString::FromInt(_iWave));

  pWaveNumberText->SetText(newWaveText);

}

void UGlobalGameUI::UpdateScoreText(int _iScore)
{
  FText newScoreText = FText::FromString("Score:  " + FString::FromInt(_iScore));

  pScoreText->SetText(newScoreText);
}

void UGlobalGameUI::UpdateGlobalUI_Visibility(bool _gamePlayGlobal, bool _checkGlobal)
{
  if (_gamePlayGlobal)
  {
    pGameplayUIHorizontalBox->SetVisibility(ESlateVisibility::Visible);
    pWaveNumberText->SetVisibility(ESlateVisibility::Visible);
    pScoreText->SetVisibility(ESlateVisibility::Visible);
  }
  else
  {
    pGameplayUIHorizontalBox->SetVisibility(ESlateVisibility::Hidden);
    pWaveNumberText->SetVisibility(ESlateVisibility::Hidden);
    pScoreText->SetVisibility(ESlateVisibility::Hidden);
  }

  if (_checkGlobal)
  {
    //pReadyPlayersHorizontalBox->SetVisibility(ESlateVisibility::Visible);
    //pReadyPlayersText->SetVisibility(ESlateVisibility::Visible);
    pReadyCanvasBox->SetVisibility(ESlateVisibility::Visible);
  }
  else
  {
    //pReadyPlayersHorizontalBox->SetVisibility(ESlateVisibility::Hidden);
    //pReadyPlayersText->SetVisibility(ESlateVisibility::Hidden);
    pReadyCanvasBox->SetVisibility(ESlateVisibility::Hidden);
  }
}
