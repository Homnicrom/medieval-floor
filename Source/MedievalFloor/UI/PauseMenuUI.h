// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "../Managers/LevelManager.h"
#include "../Managers/UIManager.h"
#include "PauseMenuUI.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UPauseMenuUI : public UUserWidget
{
	GENERATED_BODY()
	
private:

	//T�tulo del men� de pausa
	UPROPERTY(meta = (BindWidget))
		UTextBlock* pPauseMenu_Text;

	//Bot�n  para reanudar el juego
	UPROPERTY(meta = (BindWidget))
		UButton* pResumeButton;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* pResumeButton_Text;

	//Bot�n de salir al menu principal
	UPROPERTY(meta = (BindWidget))
		UButton* pExitButton;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* pExitButton_Text;

	ULevelManager* pLevelManager;

	UUIManager* pUIManager;

public:

	//Parecido a BeginPlay pero para widgets
	virtual void NativeConstruct() override;

	//Funcionalidad del bot�n de reanudar.
	UFUNCTION()
		void ResumeGame();

	//Funcionalidad del bot�n de salir al menu principal
	UFUNCTION()
		void ExitToMainMenu();
};
