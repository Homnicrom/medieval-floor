// Fill out your copyright notice in the Description page of Project Settings.


#include "GamePlayerUI.h"

#include "MedievalFloor/Characters/PlayerCharacter.h"

void UGamePlayerUI::NativeConstruct()
{
	Super::NativeConstruct();

  bDamaged = false;

  APawn* pawn = GetOwningPlayerPawn();
  APlayerCharacter* player = Cast<APlayerCharacter>(pawn);

  UpdateHealthBarPercent(player->GetPlayerCurrentHealth(), player->GetCharaterHealth());
  fCurrentPercent = static_cast<float>(player->GetPlayerCurrentHealth()) / static_cast<float>(player->GetCharaterHealth());

}

void UGamePlayerUI::UpdateHealthBarPercent(int _iCurrentHealth, int _iTotalHealth)
{
	//Si hay tiempo se podr�a investigar como bajar la vida de forma suave.
  float fPercent = static_cast<float>(_iCurrentHealth) / static_cast<float>(_iTotalHealth);

  if (fPercent > fCurrentPercent)
  {
    bDamaged = false;
  }
  else if (fPercent < fCurrentPercent)
  {
    bDamaged = true;
  }
  //pHealth_Bar->SetPercent(fPercent);
  fTargetPercent = fPercent;

  FText percentText = FText::FromString(FString::FromInt(_iCurrentHealth) + "/" + FString::FromInt(_iTotalHealth));

  pPercentHealthText->SetText(percentText);
}

void UGamePlayerUI::NativeTick(const FGeometry& MyGeometry, float DeltaTime)
{
  Super::NativeTick(MyGeometry, DeltaTime);

  if (fCurrentPercent < fTargetPercent && !bDamaged)
  {
    fCurrentPercent = fCurrentPercent + fUpdateVelocity * DeltaTime;
  }
  else if (fCurrentPercent > fTargetPercent && bDamaged)
  {
    fCurrentPercent = fCurrentPercent - fUpdateVelocity * DeltaTime;
  }

 if (fCurrentPercent_Enemy > fTargetPercent_Enemy)
  {
    fCurrentPercent_Enemy = fCurrentPercent_Enemy - fUpdateVelocity * DeltaTime;
  }

  pHealth_Bar->SetPercent(fCurrentPercent);
  pHealth_Enemy_Bar->SetPercent(fCurrentPercent_Enemy);
}

void UGamePlayerUI::ShowEnemyUI(bool _bVisible, int _iCurrentHealth, int iTotalHealth, FName _enemyType)
{
  if (_bVisible)
  {
    pEnemyTarget_Canvas->SetVisibility(ESlateVisibility::Visible);

    float fPercent = static_cast<float>(_iCurrentHealth) / static_cast<float>(iTotalHealth);
    FText percentEnemyHealthText = FText::FromString(FString::FromInt(static_cast<int>(fPercent * 100)) + "%");
    FText enemyNameText = Set_EnemyName(_enemyType.ToString());

    fTargetPercent_Enemy = fPercent;
    fCurrentPercent_Enemy = fPercent;

    pHealth_Enemy_Bar->SetPercent(fPercent);
    pPercentEnemyHealthText->SetText(percentEnemyHealthText);
    pNameEnemyText->SetText(enemyNameText);
  }
  else
  {
    pEnemyTarget_Canvas->SetVisibility(ESlateVisibility::Hidden);
  }
}

void UGamePlayerUI::UpdateEnemyUI(int _iCurrentHealth, int iTotalHealth)
{
  float fPercent = static_cast<float>(_iCurrentHealth) / static_cast<float>(iTotalHealth);
  FText percentEnemyHealthText = FText::FromString(FString::FromInt(static_cast<int>(fPercent * 100)) + "%");

  fTargetPercent_Enemy = fPercent;

  //pHealth_Enemy_Bar->SetPercent(fPercent);
  pPercentEnemyHealthText->SetText(percentEnemyHealthText);
}

FText UGamePlayerUI::Set_EnemyName(FString _enemyType)
{
  FText nameResult = FText::FromString(FString("None"));

  if (_enemyType == "Weak")
  {
    nameResult = FText::FromString(FString("Warrior"));
  }
  else if (_enemyType == "Big")
  {
    nameResult = FText::FromString(FString("Armored Warrior"));
  }
  else if (_enemyType == "Fast")
  {
    nameResult = FText::FromString(FString("Rogue"));
  }
  else if (_enemyType == "Archer")
  {
    nameResult = FText::FromString(FString("Archer"));
  }

  return nameResult;
}
