// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerControllerCheckUI.h"

#include "Kismet/GameplayStatics.h"
#include "Styling/SlateColor.h"

void UPlayerControllerCheckUI::NativeConstruct()
{
  Super::NativeConstruct();
  //Obtener el id de jugador para plasmarlo
  APlayerController* pPlayerController = GetOwningPlayer();
  int32 iControllerID = UGameplayStatics::GetPlayerControllerID(pPlayerController);

  FText playerNameText = FText::FromString(FString::Printf(TEXT("Player %d"), iControllerID + 1));
  FText readyText = FText::FromString("not ready");
  FLinearColor readyTextColor = FLinearColor(1.f, 0.f, 0.f, 1.f);
  FText pressButtonText = FText::FromString("Press ");
  FText pressButtonTextSeparator = FText::FromString("/");

  /*if (iControllerID != 0)
  {
    pKeyboardReadyBox->SetVisibility(ESlateVisibility::Collapsed);
    pKeyboardNotReadyBox->SetVisibility(ESlateVisibility::Collapsed);
  }*/

  pPlayerName_Text->SetText(playerNameText);
  pReady_Text->SetText(readyText);
  pReady_Text->SetColorAndOpacity(FSlateColor(readyTextColor));

  pPressButton_Text_Ready->SetText(pressButtonText);
  pPressButton_Text_NotReady->SetText(pressButtonText);

  pPressButton_TextSeparator_Ready->SetText(pressButtonTextSeparator);
  pPressButton_TextSeparator_NotReady->SetText(pressButtonTextSeparator);

  pReadyHorizontalBox->SetVisibility(ESlateVisibility::Hidden);
  pNotReadyHorizontalBox->SetVisibility(ESlateVisibility::Visible);
}

void UPlayerControllerCheckUI::UpdateReadyState(bool _bReady)
{
  if (_bReady)
  {
    FText readyText = FText::FromString("ready!");
    FLinearColor readyTextColor = FLinearColor(0.f, 1.f, 0.f, 1.f);

    pReady_Text->SetText(readyText);
    pReady_Text->SetColorAndOpacity(FSlateColor(readyTextColor));
    pReadyHorizontalBox->SetVisibility(ESlateVisibility::Visible);
    pNotReadyHorizontalBox->SetVisibility(ESlateVisibility::Hidden);
  }
  else
  {
    FText readyText = FText::FromString("not ready");
    FLinearColor readyTextColor = FLinearColor(1.f, 0.f, 0.f, 1.f);

    pReady_Text->SetText(readyText);
    pReady_Text->SetColorAndOpacity(FSlateColor(readyTextColor));

    pReadyHorizontalBox->SetVisibility(ESlateVisibility::Hidden);
    pNotReadyHorizontalBox->SetVisibility(ESlateVisibility::Visible);
  }
}
