// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "../Managers/UIManager.h"
#include "GamePlayerUI.h"
#include "PlayerControllerCheckUI.h"
#include "../Managers/LevelManager.h"
#include "../Managers/UIManager.h"
#include "../Enemies/EnemyClass/EnemiesBase.h"
#include "PlayerHUD.generated.h"


/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API APlayerHUD : public AHUD
{
	GENERATED_BODY()

private:

	UUIManager* pUIManager;

	ULevelManager* pLevelManager;

	UGamePlayerUI* pWidgetUI;
	
	UPlayerControllerCheckUI* pCheckUI;

	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> gamePlayerBP;

	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> checkUIBP;

  APlayerCharacter* pMyPlayer;

  AEnemiesBase* pPreviousEnemy;
  
  float EnemyUI_Timer;

protected:

	virtual void BeginPlay() override;

  virtual void Tick(float DeltaSeconds) override;

public:

	//Lo mismo que en GamePlayerUI (revisar m�s tarde)
	UFUNCTION()
		void UpdateHealth(APlayerCharacter* _pDamagedCharacter);


	UFUNCTION()
		void LoadControllerCheckUI();

	UFUNCTION()
		void HideControllerCheckUI();

	UFUNCTION()
		void UpdateControllerCheckUI(bool _readyStatus);

  UFUNCTION()
    AEnemiesBase* Raycast_Enemy();

  UFUNCTION()
    void UpdateGamePlayerUI_Enemy(AEnemiesBase* _damagedEnemy);

};
