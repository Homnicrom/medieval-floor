// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/HorizontalBox.h"
#include "Components/Button.h"
#include "../Managers/LevelManager.h"
#include "GameOverUI.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UGameOverUI : public UUserWidget
{
	GENERATED_BODY()
	
private:

	//Aqu� enlazo los elementos que hay en el BP de esta interfaz a estas variables para poder modificar los valores de los widgets dinamicamente.

	UPROPERTY(meta = (BindWidget))
		UTextBlock* pGameOver_Text;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* pFinalScore_Text;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* pScore_Data_Text;

  UPROPERTY(meta = (BindWidget))
    UButton* pExitButton;

  UPROPERTY(meta = (BindWidget))
    UTextBlock* pExitButtonText;

  UPROPERTY(meta = (BindWidget))
    UHorizontalBox* pHighestScore_HorizontalBox;

  //Animaciones
  UPROPERTY(Transient, meta = (BindWidgetAnim))
    class UWidgetAnimation* fadeInAnim;

  UPROPERTY()
  ULevelManager* pLevelManager;

  FTimerHandle timerHandle;

public:
	//Parecido a BeginPlay pero para widgets
	virtual void NativeConstruct() override;

  UFUNCTION()
    void SetFinalScore_HighestScoreMsg(int _iScore, bool _isHighScore);

  UFUNCTION()
    void ExitToMainMenu();

  UFUNCTION()
    void SetWidgetVisibility();


  UFUNCTION()
    void InitGameOver();
};
