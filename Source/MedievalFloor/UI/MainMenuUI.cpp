// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuUI.h"
#include "Kismet/GameplayStatics.h"

void UMainMenuUI::NativeConstruct()
{
	Super::NativeConstruct();
	//Esta ruta de buscado habr� que modificarla dependiendo de d�nde pongamos al level manager
  bInteractable = true;

	TArray<AActor*> outActors;
	UGameplayStatics::GetAllActorsWithTag(this, "Manager", outActors);

	//S�lo deber�a haber 1 actor con el tag Manager.
	pLevelManagerRef = Cast<ULevelManager>(outActors[0]->GetComponentByClass(ULevelManager::StaticClass()));

	FText gameTitle = FText::FromString("MedievalFloor");
	FText playText = FText::FromString("Play");
	FText exitText = FText::FromString("Exit");

	pGameTitle->SetText(gameTitle);
	pPlayButton_Text->SetText(playText);
	pExitButton_Text->SetText(exitText);


	pPlayButton->OnClicked.AddDynamic(this, &UMainMenuUI::PlayGameEvent);
	pExitButton->OnClicked.AddDynamic(this, &UMainMenuUI::ExitGameEvent);


}

void UMainMenuUI::PlayGameEvent()
{
  if (bInteractable)
  {
    PlayAnimation(fadeOutAnim);
    pLevelManagerRef->LoadLevel();
    bInteractable = false;
  }
  
}

void UMainMenuUI::ExitGameEvent()
{
  if (bInteractable)
  {
    pLevelManagerRef->Exit();
    bInteractable = false;
  }
}

void UMainMenuUI::InitMainMenu()
{
  SetVisibility(ESlateVisibility::Visible);
  PlayAnimationReverse(fadeOutAnim);
}

void UMainMenuUI::SetHighestScore(int _iHighestScore)
{
  FText highestScoreText = FText::FromString("Highest Score:  " + FString::FromInt(_iHighestScore));

  pHighestScore_Text->SetText(highestScoreText);
}