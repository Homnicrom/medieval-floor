// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerHUD.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraComponent.h"
#include "../GameMode/GameplayGameMode.h"
#include "DrawDebugHelpers.h"

void APlayerHUD::BeginPlay()
{
  Super::BeginPlay();

  pPreviousEnemy = nullptr;
  EnemyUI_Timer = 0.f;

  TArray<AActor*> outActors;

  UGameplayStatics::GetAllActorsWithTag(this, FName("Manager"), outActors);

  pLevelManager = Cast<ULevelManager>(outActors[0]->GetComponentByClass(ULevelManager::StaticClass()));
  pUIManager = Cast<UUIManager>(outActors[0]->GetComponentByClass(UUIManager::StaticClass()));

  if (pLevelManager->GetCurrentLevel() == 1)
  {
    pCheckUI = Cast<UPlayerControllerCheckUI>(CreateWidget(PlayerOwner, checkUIBP));

    pCheckUI->AddToPlayerScreen(1.f);
    pCheckUI->SetVisibility(ESlateVisibility::Hidden);

    if (!pUIManager->bAllPlayersReady)
    {
      LoadControllerCheckUI();
    }

    //Binding to messages
    AGameplayGameMode* pGameMode = Cast<AGameplayGameMode>(UGameplayStatics::GetGameMode(this));

    pGameMode->GameplayReadyEventDelegate.AddDynamic(this, &APlayerHUD::HideControllerCheckUI);

  }
}

void APlayerHUD::Tick(float DeltaSeconds)
{
  Super::Tick(DeltaSeconds);

  AEnemiesBase* pEnemy = nullptr;

  if (pUIManager->bAllPlayersReady)
  {
    pMyPlayer = Cast<APlayerCharacter>(GetOwningPawn());
    pEnemy = Raycast_Enemy();

    if (pEnemy != nullptr)
    {
      if (pEnemy != pPreviousEnemy)
      {
        if (pPreviousEnemy != nullptr)
        {
          pPreviousEnemy->damageEvent.RemoveDynamic(this, &APlayerHUD::UpdateGamePlayerUI_Enemy);
        }
        pEnemy->damageEvent.AddDynamic(this, &APlayerHUD::UpdateGamePlayerUI_Enemy);

        pPreviousEnemy = pEnemy;

        pWidgetUI->ShowEnemyUI(true, pEnemy->GetCurrentHealth(), pEnemy->GetMaxHealth(), pEnemy->characterTypeName);
      }
    }
    else
    {
      if (EnemyUI_Timer >= 1.f)
      {
        EnemyUI_Timer = 0.f;

        if (pPreviousEnemy != nullptr)
        {
          pPreviousEnemy->damageEvent.RemoveDynamic(this, &APlayerHUD::UpdateGamePlayerUI_Enemy);
        }

        pPreviousEnemy = nullptr;
        pWidgetUI->ShowEnemyUI(false);
      }

      EnemyUI_Timer = EnemyUI_Timer + DeltaSeconds;
    }
  }
}

AEnemiesBase* APlayerHUD::Raycast_Enemy()
{
  AEnemiesBase* pEnemy = nullptr;
  FVector camLocation = pMyPlayer->ThirdPersonCameraComponent->GetComponentLocation();
  FVector camForward = pMyPlayer->ThirdPersonCameraComponent->GetForwardVector();

  FVector ray_Start = camLocation;
  FVector ray_End = camLocation + (camForward * 1000000.f);

  FHitResult Hit;

  FCollisionQueryParams collisionQueryConfig;

  collisionQueryConfig.AddIgnoredActor(pMyPlayer);


  GetWorld()->LineTraceSingleByChannel(Hit, ray_Start, ray_End, ECollisionChannel::ECC_GameTraceChannel3, collisionQueryConfig);

  //DrawDebugLine(GetWorld(), ray_Start, ray_End, FColor::Blue);

  if (Hit.GetActor() != nullptr)
  {
    if (Hit.GetActor()->ActorHasTag(FName("Enemy")))
    {
      pEnemy = Cast< AEnemiesBase>(Hit.GetActor());
      return pEnemy;
    }
  }

  return pEnemy;
}

void APlayerHUD::UpdateHealth(APlayerCharacter* _pDamagedCharacter)
{
  APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetOwningPawn());
  int iTotalHealth = playerCharacter->GetCharaterHealth();
  int iCurrentHealth = playerCharacter->GetPlayerCurrentHealth();

  float fCurrentHealthPercent = iCurrentHealth / iTotalHealth;

  pWidgetUI->UpdateHealthBarPercent(iCurrentHealth, iTotalHealth);

  if (_pDamagedCharacter == 0)
  {
    _pDamagedCharacter->healthChangedEvent.RemoveDynamic(this, &APlayerHUD::UpdateHealth);
  }
}

void APlayerHUD::LoadControllerCheckUI()
{
  pCheckUI->SetVisibility(ESlateVisibility::Visible);
}

void APlayerHUD::HideControllerCheckUI()
{
  pCheckUI->SetVisibility(ESlateVisibility::Hidden);

  APlayerCharacter* pawn = Cast<APlayerCharacter>(GetOwningPawn());

  pWidgetUI = Cast<UGamePlayerUI>(CreateWidget(PlayerOwner, gamePlayerBP));
  pWidgetUI->AddToPlayerScreen();
  pawn->healthChangedEvent.AddDynamic(this, &APlayerHUD::UpdateHealth);
}

void APlayerHUD::UpdateControllerCheckUI(bool _readyStatus)
{
  pCheckUI->UpdateReadyState(_readyStatus);
}

void APlayerHUD::UpdateGamePlayerUI_Enemy(AEnemiesBase* _damagedEnemy)
{
  pWidgetUI->UpdateEnemyUI(_damagedEnemy->GetCurrentHealth(), _damagedEnemy->GetMaxHealth());
}