// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "../Managers/LevelManager.h"
#include "MainMenuUI.generated.h"


/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UMainMenuUI : public UUserWidget
{
	GENERATED_BODY()

private:

	//Aqu� enlazo los elementos que hay en el BP de esta interfaz a estas variables para poder modificar los valores de los widgets dinamicamente adem�s
	//de a�adirle funcionalidad a los botones.

	//T�tulo del main menu
	UPROPERTY(meta = (BindWidget))
		UTextBlock* pGameTitle;
	//Bot�n de jugar
	UPROPERTY(meta = (BindWidget))
		UButton* pPlayButton;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* pPlayButton_Text;

	//Bot�n de salir del juego
	UPROPERTY(meta = (BindWidget))
		UButton* pExitButton;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* pExitButton_Text;

  UPROPERTY(meta = (BindWidget))
    UTextBlock* pHighestScore_Text;

	//Animaciones
	UPROPERTY(Transient, meta = (BindWidgetAnim))
		class UWidgetAnimation* fadeOutAnim;

	//Referencia al levelManager [Comunicaci�n 1:1]
	ULevelManager* pLevelManagerRef;

  bool bInteractable;



public:
	//Parecido a BeginPlay pero para widgets
	virtual void NativeConstruct() override;
	
	//Comportamiento del bot�n de jugar. (Msg)
	UFUNCTION()
	void PlayGameEvent();

	//Comportamiento del bot�n de salir. (Msg)
	UFUNCTION()
	void ExitGameEvent();

  UFUNCTION(BlueprintCallable)
    void InitMainMenu();

  UFUNCTION()
    void SetHighestScore(int _iHighestScore);
};
