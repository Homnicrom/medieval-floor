// Fill out your copyright notice in the Description page of Project Settings.


#include "GameOverUI.h"
#include "Kismet/GameplayStatics.h"

void UGameOverUI::NativeConstruct()
{
	Super::NativeConstruct();

  TArray<AActor*> outActors;
  UGameplayStatics::GetAllActorsWithTag(this, FName("Manager"), outActors);

  pLevelManager = Cast<ULevelManager>(outActors[0]->GetComponentByClass(ULevelManager::StaticClass()));

	FText gameOverText = FText::FromString("Game Over");
	FText finalScoreText = FText::FromString("Your score is:");
  FText exitButtonText = FText::FromString("Exit");

	pGameOver_Text->SetText(gameOverText);
	pFinalScore_Text->SetText(finalScoreText);
  pExitButtonText->SetText(exitButtonText);

	FText scoreDataText = FText::FromString("--");

	//pScore_Data_Text->SetText(scoreDataText);

  pExitButton->OnClicked.AddDynamic(this, &UGameOverUI::ExitToMainMenu);
}

void UGameOverUI::ExitToMainMenu()
{
  pLevelManager->LoadLevel();
}


void UGameOverUI::SetFinalScore_HighestScoreMsg(int _iScore, bool _isHighScore)
{
  FText scoreDataText = FText::FromString(FString::FromInt(_iScore));

  pScore_Data_Text->SetText(scoreDataText);

  if (_isHighScore)
  {
    pHighestScore_HorizontalBox->SetVisibility(ESlateVisibility::Visible);
  }
  else
  {
    pHighestScore_HorizontalBox->SetVisibility(ESlateVisibility::Hidden);
  }
}

void UGameOverUI::SetWidgetVisibility()
{
  SetVisibility(ESlateVisibility::Visible);
  PlayAnimation(fadeInAnim);
}

void UGameOverUI::InitGameOver()
{
  SetVisibility(ESlateVisibility::Hidden);
  GetOwningPlayer()->GetWorldTimerManager().SetTimer(timerHandle, this, &UGameOverUI::SetWidgetVisibility, 2.5f, false);
}