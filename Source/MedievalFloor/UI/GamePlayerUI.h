// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"
#include "Components/CanvasPanel.h"
#include "GamePlayerUI.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UGamePlayerUI : public UUserWidget
{
	GENERATED_BODY()

private:

	//Aqu� enlazo los elementos que hay en el BP de esta interfaz a estas variables para poder modificar los valores de los widgets dinamicamente.

	UPROPERTY(meta = (BindWidget))
		UProgressBar* pHealth_Bar;

  UPROPERTY(meta = (BindWidget))
    UTextBlock* pPercentHealthText;

  UPROPERTY(meta = (BindWidget))
    UCanvasPanel* pEnemyTarget_Canvas;

  UPROPERTY(meta = (BindWidget))
    UProgressBar* pHealth_Enemy_Bar;

  UPROPERTY(meta = (BindWidget))
    UTextBlock* pPercentEnemyHealthText;

  UPROPERTY(meta = (BindWidget))
    UTextBlock* pNameEnemyText;

  float fCurrentPercent;

  float fTargetPercent;

  float fCurrentPercent_Enemy;

  float fTargetPercent_Enemy;
  
  UPROPERTY(EditAnywhere)
    float fUpdateVelocity;

  bool bDamaged;

protected:
  virtual void NativeTick(const FGeometry& MyGeometry, float DeltaTime) override;

public:

	//Parecido a BeginPlay pero para widgets
	virtual void NativeConstruct() override;

	//Actualiza la interfaz de vida.
	//Me pueden pasar el porcentaje o la vida actual junto a la vida total.
	UFUNCTION()
		void UpdateHealthBarPercent(int _iCurrentHealth, int _iTotalHealth);

  UFUNCTION()
    void ShowEnemyUI(bool _bVisible, int _iCurrentHealth = 0, int iTotalHealth = 0, FName _enemyType = "NONE");

  UFUNCTION()
    void UpdateEnemyUI(int _iCurrentHealth, int _iTotalHealth);

  FText Set_EnemyName(FString _enemyType);
	
};
