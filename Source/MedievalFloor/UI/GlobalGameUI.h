// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/HorizontalBox.h"
#include "Components/CanvasPanel.h"
#include "GlobalGameUI.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UGlobalGameUI : public UUserWidget
{
	GENERATED_BODY()

private:

  UPROPERTY(meta = (BindWidget))
    UHorizontalBox* pGameplayUIHorizontalBox;

  UPROPERTY(meta = (BindWidget))
    UTextBlock* pWaveNumberText;

  UPROPERTY(meta = (BindWidget))
    UTextBlock* pScoreText;

  //UPROPERTY(meta = (BindWidget))
  //  UHorizontalBox* pReadyPlayersHorizontalBox;

  //UPROPERTY(meta = (BindWidget))
    //UTextBlock* pReadyPlayersText;

  UPROPERTY(meta = (BindWidget))
    UCanvasPanel* pReadyCanvasBox;

public:
  //Parecido a BeginPlay pero para widgets
  virtual void NativeConstruct() override;

  UFUNCTION()
    void UpdateWaveText(int _iWave);

  UFUNCTION()
    void UpdateScoreText(int _iScore);

  UFUNCTION()
    void UpdateGlobalUI_Visibility(bool _gamePlayGlobal, bool _checkGlobal);
};
