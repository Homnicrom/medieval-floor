// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/CanvasPanel.h"
#include "Components/HorizontalBox.h"
#include "PlayerControllerCheckUI.generated.h"

/**
 * 
 */
UCLASS()
class MEDIEVALFLOOR_API UPlayerControllerCheckUI : public UUserWidget
{
  GENERATED_BODY()

private:

  UPROPERTY(meta = (BindWidget))
  UTextBlock* pPlayerName_Text;

  UPROPERTY(meta = (BindWidget))
  UTextBlock* pReady_Text;

  UPROPERTY(meta = (BindWidget))
  UHorizontalBox* pNotReadyHorizontalBox;

  UPROPERTY(meta = (BindWidget))
  UTextBlock* pPressButton_Text_NotReady;

  UPROPERTY(meta = (BindWidget))
  UTextBlock* pPressButton_TextSeparator_NotReady;

  UPROPERTY(meta = (BindWidget))
  UHorizontalBox* pReadyHorizontalBox;

  UPROPERTY(meta = (BindWidget))
  UTextBlock* pPressButton_Text_Ready;

  UPROPERTY(meta = (BindWidget))
  UTextBlock* pPressButton_TextSeparator_Ready;

  UPROPERTY(meta = (BindWidget))
  UHorizontalBox* pKeyboardReadyBox;

  UPROPERTY(meta = (BindWidget))
  UHorizontalBox* pKeyboardNotReadyBox;

public:

  virtual void NativeConstruct() override;

  UFUNCTION()
  void UpdateReadyState(bool _bReady);
};
