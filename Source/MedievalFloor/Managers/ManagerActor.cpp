// Fill out your copyright notice in the Description page of Project Settings.


#include "ManagerActor.h"

// Sets default values
AManagerActor::AManagerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Tags.Add(FName("Manager"));
	pLevelManager = CreateDefaultSubobject<ULevelManager>(TEXT("LevelManager"));
	pUIManager = CreateDefaultSubobject<UUIManager>(TEXT("UIManager"));

}

// Called when the game starts or when spawned
void AManagerActor::BeginPlay()
{
	Super::BeginPlay();
	//Controlamos el orden de la inicialización de los componentes de este actor.

	pLevelManager->LevelInit();

  if (pLevelManager->GetCurrentLevel() == 0)
  {
    pUIManager->InitUI();
  }
}

// Called every frame
void AManagerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

