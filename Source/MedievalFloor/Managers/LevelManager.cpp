// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelManager.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/GameModeBase.h"

// Sets default values for this component's properties
ULevelManager::ULevelManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	levelsList = { "MainMenu_Map", "Battleground_Map" };
	fDelay = 3.5f;

	// ...
}


// Called when the game starts
void ULevelManager::BeginPlay()
{
	Super::BeginPlay();

	//Falta empezar a escuchar el evento del GameMode


	LevelInit();

	// ...
	
}


// Called every frame
void ULevelManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void ULevelManager::LoadLevel(FName _levelName)
{

	if (_levelName.IsEqual(FName("NONE")))
	{
		_levelName = GetNextLevel();

    if (GetCurrentLevel() == 0)
    {
      OnLevelExit();
      timerDelegate.BindUFunction(this, FName("LoadLevelAfterDelay"), _levelName);
      GetOwner()->GetWorldTimerManager().SetTimer(timerDelay, timerDelegate, fDelay, false);
      UE_LOG(LogTemp, Warning, TEXT("Loading Level: %s"), *_levelName.ToString());
    }
    else
    {
      LoadLevelAfterDelay(_levelName);
    }
	}
	else
	{
		if (levelsList.Find(_levelName) != -1)
		{
      if (GetCurrentLevel() == 0)
      {
        OnLevelExit();
        timerDelegate.BindUFunction(this, FName("LoadLevelAfterDelay"), _levelName);
        GetOwner()->GetWorldTimerManager().SetTimer(timerDelay, timerDelegate, fDelay, false);
        UE_LOG(LogTemp, Warning, TEXT("Loading Level: %s"), *_levelName.ToString());
      }
      else
      {
        LoadLevelAfterDelay(_levelName);
      }
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Cannot load %s. Does it exists?"), *_levelName.ToString());
		}
	}

}


void ULevelManager::LoadLevelAfterDelay(FName _levelName)
{
  OnChangeLevelEventDelegate.Broadcast();
	UGameplayStatics::OpenLevel(this, _levelName);
}


FName ULevelManager::GetNextLevel()
{
	int iCurrentIndex = levelsList.IndexOfByKey(currentLevel);
	
	iCurrentIndex = (iCurrentIndex + 1) % levelsList.Num();

	FName result = levelsList[iCurrentIndex];

	return result;
}

void ULevelManager::OnLevelExit()
{
	//CUIDADO! Se considera el uso de una c�mara general. Habr� que probarlo y adaptarlo para 4 viewports.
	FLinearColor cBlack = FLinearColor(0.f, 0.f, 0.f, 1.f);
	int iPlayers = UGameplayStatics::GetGameMode(this)->GetNumPlayers();

	if (iPlayers == 1)
	{
		UGameplayStatics::GetPlayerCameraManager(this, 0)->StartCameraFade(0.f, 1.f, fDelay, cBlack, false, true);
	}
	else
	{

	}
}

void ULevelManager::LevelInit()
{
	currentLevel = FName(UGameplayStatics::GetCurrentLevelName(this));
	//CUIDADO! Se considera el uso de una c�mara general. Habr� que probarlo y adaptarlo para 4 viewports.
	FLinearColor cBlack = FLinearColor(0.f, 0.f, 0.f, 1.f);
	int iPlayers = UGameplayStatics::GetGameMode(this)->GetNumPlayers();

	if (iPlayers == 1)
	{
		UGameplayStatics::GetPlayerCameraManager(this, 0)->StartCameraFade(1.f, 0.f, fDelay, cBlack, false, true);
	}
	else
	{

	}
}

void ULevelManager::Exit()
{
	UKismetSystemLibrary::QuitGame(this, nullptr, EQuitPreference::Quit, true);
}

int ULevelManager::GetCurrentLevel()
{
	return levelsList.IndexOfByKey(currentLevel);
}

