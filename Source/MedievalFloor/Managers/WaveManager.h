// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MedievalFloor/Enemies/EnemyClass/EnemiesBase.h"
#include "MedievalFloor/Enemies/Extra/EnemyDifficultyStruct.h"

#include "WaveManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FNewWaveEventSignature, int, _waveNumber);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUpdateScoreEventSignature, int, _currentScore);

UCLASS()
class MEDIEVALFLOOR_API UWaveManager : public UObject
{
  GENERATED_BODY()

public:
  UWaveManager();
  void Init(TArray<FEnemyDifficultyStruct> _enemiesList, TArray<AActor*> _enemySpawnPoints, float _secondsBetweenWaves, float _timeBetweenEnemySpawns, int _difficultyIncreaseBetweenWaves, int _startingDifficultyLevel);
  ~UWaveManager();
  void Start();
  void Stop();

  int GetScore();

  UPROPERTY(BlueprintAssignable)
  FNewWaveEventSignature NewWaveEventDelegate;

  UPROPERTY(BlueprintAssignable)
  FUpdateScoreEventSignature UpdateScoreEventDelegate;

private:
  bool isInit = false;
  bool bIsWaveStarting = true;

  int iScore = 0;

  unsigned int iSpawnedCount = 0;

  float fTimeBetweenWaves = 15;
  float fTimeBetweenEnemySpawns = 0.5f;
  uint8 uDifficultyIncreaseBetweenWaves = 5;

  //variables para controlar las primeras oleadas
  uint8 uArrayCapIndex = 0;

  uint8 uCurrentWave = 1;
  uint8 uCurrentDifficultyLevel = 0;

  FTimerHandle TimerHandle;

  TArray<FEnemyDifficultyStruct> enemiesArray;
  TArray<FEnemyDifficultyStruct> enemiesToGenerate;
  TArray<AActor*> enemySpawnPoints;
  TArray<AActor*> spawnPool;

  bool* enemiesAssignedToSpawn = nullptr;
  bool allSpawnsAssigned = false;

  UFUNCTION()
  void SpawnEnemiesTimer();

  UFUNCTION()
  void OnEnemyDead(AEnemiesBase* _deadEnemy);

  void NextWave();

  void GenerateNextWave(uint8 _uWaveDifficulty);
  void CheckAndUpdateArrayCap(unsigned int* pArrayCap_, unsigned int _iRemainingDifficulty);
};
