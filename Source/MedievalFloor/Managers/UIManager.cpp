// Fill out your copyright notice in the Description page of Project Settings.


#include "UIManager.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
#include "../UI/PauseMenuUI.h"
#include "../UI/PlayerHUD.h"
#include "../GameInstance/MedievalFloorGameInstance.h"


// Sets default values for this component's properties
UUIManager::UUIManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	pLevelManager = nullptr;
	pMenuWidget = nullptr;
	pPauseWidget = nullptr;
	pGameOverWidget = nullptr;
  pGlobalGameWidget = nullptr;
	bAllPlayersReady = false;
  iNumberPlayersReady = 0;
  isInitialized = false;
	// ...
}


// Called when the game starts
void UUIManager::BeginPlay()
{
	Super::BeginPlay();

	pLevelManager = Cast<ULevelManager>(GetOwner()->GetComponentByClass(ULevelManager::StaticClass()));

	//Binding messages
  if (pLevelManager->GetCurrentLevel() == 1)
  {
    pGameMode = Cast<AGameplayGameMode>(UGameplayStatics::GetGameMode(this));

    pGameMode->PlayerReadyEventDelegate.AddDynamic(this, &UUIManager::UpdatePlayer_CheckControllerUI);
    pGameMode->GameplayReadyEventDelegate.AddDynamic(this, &UUIManager::SetAllPlayersReady);
    pGameMode->GamePausedEventDelegate.AddDynamic(this, &UUIManager::LoadPauseUI);
    pGameMode->GameOverEventDelegate.AddDynamic(this, &UUIManager::LoadGameOverUI);
    
  }
	// ...
	
}


// Called every frame
void UUIManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

UMainMenuUI* UUIManager::GetMainMenuWidget()
{
	return pMenuWidget;
}

UPauseMenuUI* UUIManager::GetPauseMenuWidget()
{
	return pPauseWidget;
}

void UUIManager::InitUI()
{
	APlayerController* pDefaultController = UGameplayStatics::GetPlayerController(this, 0);

  if(!pDefaultController)
  {
    pDefaultController = UGameplayStatics::CreatePlayer(this, 0, true);
  }

	if (pLevelManager->GetCurrentLevel() == 0)
	{
		pDefaultController->bShowMouseCursor = true;
		pDefaultController->SetIgnoreLookInput(true);
		pDefaultController->SetInputMode(FInputModeGameOnly());
		//Asumimos que hay un player con un viewport.
		pMenuWidget = Cast<UMainMenuUI>(CreateWidget(pDefaultController, mainMenuBP, FName("MainMenu")));
		pMenuWidget->AddToViewport();
    pMenuWidget->SetVisibility(ESlateVisibility::Hidden);

    int iHighestScore = Cast<UMedievalFloorGameInstance>(UGameplayStatics::GetGameInstance(this))->GetHighScore();
    pMenuWidget->SetHighestScore(iHighestScore);
	}
	else if (pLevelManager->GetCurrentLevel() == 1)
	{
		pPauseWidget = Cast<UPauseMenuUI>(CreateWidget(pDefaultController, pauseMenuBP, FName("PauseMenu")));
		pPauseWidget->AddToViewport(99);
		
		pPauseWidget->SetVisibility(ESlateVisibility::Hidden);

    pGlobalGameWidget = Cast<UGlobalGameUI>(CreateWidget(pDefaultController, globalGameBP, FName("GlobalGameUI")));
    pGlobalGameWidget->AddToViewport();
	}
}

void UUIManager::LoadPauseUI(bool _paused)
{
  APlayerController* pDefaultController = UGameplayStatics::GetPlayerController(this, 0);

  if (_paused)
  {
    pDefaultController->SetIgnoreLookInput(true);
    pDefaultController->SetInputMode(FInputModeGameAndUI());

    pPauseWidget->SetVisibility(ESlateVisibility::Visible);
    pDefaultController->bShowMouseCursor = true;
    //pPauseWidget->ShowCursor(true);


    UE_LOG(LogTemp, Warning, TEXT("bShowCursor: %d"), pDefaultController->bShowMouseCursor);
  }
  else
  {
    pDefaultController->SetIgnoreLookInput(false);
    pDefaultController->SetInputMode(FInputModeGameOnly());

    pPauseWidget->SetVisibility(ESlateVisibility::Hidden);
    pDefaultController->bShowMouseCursor = false;
    //pPauseWidget->ShowCursor(false);
    UE_LOG(LogTemp, Warning, TEXT("bShowCursor: %d"), pDefaultController->bShowMouseCursor);
  }
}

void UUIManager::LoadGameOverUI(int _score, bool _isHighScore)
{
	APlayerController* pDefaultController = UGameplayStatics::GetPlayerController(this, 0);

  pDefaultController->SetIgnoreLookInput(true);
  pDefaultController->SetInputMode(FInputModeGameAndUI());

  pDefaultController->bShowMouseCursor = true;

	pGameOverWidget = Cast<UGameOverUI>(CreateWidget(pDefaultController, gameOverBP, FName("GameOverUI")));
  pGameOverWidget->InitGameOver();
  pGameOverWidget->SetFinalScore_HighestScoreMsg(_score, _isHighScore);
	pGameOverWidget->AddToViewport();
}


void UUIManager::HideGameOverUI()
{
	pGameOverWidget->SetVisibility(ESlateVisibility::Hidden);
}

void UUIManager::UpdatePlayer_CheckControllerUI(uint8 _playerID, bool _readyStatus)
{
  
  if (!isInitialized)
  {
    //Creamos aqu� el GlobalUI unicamente.
    TemporaryInit();
    isInitialized = true;
  }

  AHUD* pHUD = pGameMode->ControllersList[_playerID]->PlayerController->GetHUD();
	APlayerHUD* pPlayerHUD = Cast<APlayerHUD>(pHUD);
  pPlayerHUD->UpdateControllerCheckUI(_readyStatus);

  if (_readyStatus)
  {
    iNumberPlayersReady++;
  }
  else
  {
    iNumberPlayersReady--;
  }

  if (iNumberPlayersReady == pGameMode->NumberOfPlayers)
  {
    pGlobalGameWidget->UpdateGlobalUI_Visibility(false, true);
  }
  else
  {
    pGlobalGameWidget->UpdateGlobalUI_Visibility(false, false);
  }
  
}

void UUIManager::SetAllPlayersReady()
{
	bAllPlayersReady = true;
  //Eliminas del viewport el antiguo global
  pGlobalGameWidget->RemoveFromViewport();
  //DESTRUCCION TEORICA DE UNREAL DEL WIDGET
  //Aqui creamos toda la interfaz no dependiente de los PlayerHUD
  InitUI();
  pGlobalGameWidget->UpdateGlobalUI_Visibility(true, false);

  pGameMode->WaveManager->NewWaveEventDelegate.AddDynamic(this, &UUIManager::UpdateWave_GlobalGameUI);
  pGameMode->WaveManager->UpdateScoreEventDelegate.AddDynamic(this, &UUIManager::UpdateScore_GlobalGameUI);
}

void UUIManager::UpdateWave_GlobalGameUI(int _waveNumber)
{
  pGlobalGameWidget->UpdateWaveText(_waveNumber);
}

void UUIManager::UpdateScore_GlobalGameUI(int _currentScore)
{
  pGlobalGameWidget->UpdateScoreText(_currentScore);
}

void UUIManager::TemporaryInit()
{
  APlayerController* pDefaultPlayerController = UGameplayStatics::GetPlayerController(this, 0);

  pGlobalGameWidget = Cast<UGlobalGameUI>(CreateWidget(pDefaultPlayerController, globalGameBP, FName("GlobalGameUI")));
  pGlobalGameWidget->AddToViewport();

}


//La funcion init por defecto que introducir� lo mismo que hasta ahora que se ejecuta cuando todos est�n ready