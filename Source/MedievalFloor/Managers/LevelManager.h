// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LevelManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangeLevelEventSignature);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MEDIEVALFLOOR_API ULevelManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULevelManager();


  UPROPERTY(BlueprintAssignable)
    FOnChangeLevelEventSignature OnChangeLevelEventDelegate;

	/*
		Lista de los diferentes niveles que tendr� el juego.
		Para a�adir un nivel habr� que introducir su nombre exacto.
	*/
	UPROPERTY(EditAnywhere)
		TArray<FName> levelsList;
	
	//Tiempo de espera desde que se llama a la funci�n @LoadLevelAfterDelay() hasta que se cambia de nivel.
	UPROPERTY(EditAnywhere)
		float fDelay;

private:

	UPROPERTY(EditAnywhere)
		FName currentLevel;
	//Utilizado para inicializar el timer de carga de nivel
	FTimerHandle timerDelay;

	//Utilizado para crear una funci�n delegada que admita par�metros.
	FTimerDelegate timerDelegate;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Funci�n que se llamar� al inciar el nivel. Pr�cticamente es lo mismo que el BeginPlay del LevelBlueprint pero desde el LevelManager.
	//Aqu� se introducen todas las acciones a realizar JUSTO al entrar al nivel.
	void LevelInit();

	// Funci�n que se encargar� de cargar un nivel. Es una interfaz para el usuario. Realmente, esperar� X segundos despu�s de llamarla y cargar� el nivel.
	// Si el par�metro opcional @_levelName no es introducido, se cargar� el siguiente nivel al actual.
	UFUNCTION(BlueprintCallable)
		void LoadLevel(FName _levelName = FName("NONE"));

	//Funci�n que cierra la aplicaci�n.
	void Exit();

	//Devuelve el �ndice del nivel actual.
	int GetCurrentLevel();

private:
	FName GetNextLevel();

	//Funci�n que carga un nivel
	UFUNCTION()
		void LoadLevelAfterDelay(FName _levelName);

	//Funci�n que se llamar� al salir del nivel. Aqu� se introducen todas las acciones a realizar ANTES de salir del nivel.
	void OnLevelExit();

		
};
