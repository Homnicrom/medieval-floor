// Fill out your copyright notice in the Description page of Project Settings.

#include "WaveManager.h"

#include "Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"
#include "MedievalFloor/OtherElements/Spawner.h"

UWaveManager::UWaveManager()
{
}

void UWaveManager::Init(TArray<FEnemyDifficultyStruct> _enemiesList, TArray<AActor*> _enemySpawnPoints, float _secondsBetweenWaves, float _timeBetweenEnemySpawns, int _difficultyIncreaseBetweenWaves, int _startingDifficultyLevel)
{
  enemySpawnPoints = _enemySpawnPoints;
  enemiesArray = _enemiesList;
  enemiesArray.Sort();

  fTimeBetweenWaves = _secondsBetweenWaves;
  fTimeBetweenEnemySpawns = _timeBetweenEnemySpawns;
  uDifficultyIncreaseBetweenWaves = _difficultyIncreaseBetweenWaves;
  uCurrentDifficultyLevel = _startingDifficultyLevel;

  isInit = true;
}

UWaveManager::~UWaveManager()
{
}

void UWaveManager::Start()
{
  NextWave();
}

void UWaveManager::Stop()
{
  UGameInstance* GameInstance = UGameplayStatics::GetGameInstance(GetWorld());
  GameInstance->GetTimerManager().ClearTimer(TimerHandle);
}

int UWaveManager::GetScore()
{
  return iScore;
}

void UWaveManager::GenerateNextWave(uint8 _uWaveDifficulty)
{
  ensure(isInit);

  uint8 iRemainingDifficulty = _uWaveDifficulty;
  unsigned int uCurrentArrayCapIndex = uArrayCapIndex;

  enemiesToGenerate.Reset();

  while (iRemainingDifficulty > 0)
  {
    CheckAndUpdateArrayCap(&uCurrentArrayCapIndex, iRemainingDifficulty);
    const unsigned int uIndexToGenerate = FMath::RandRange(0, uCurrentArrayCapIndex);
    const unsigned int uDifficultyLevel = enemiesArray[uIndexToGenerate].DifficultyLevel;
    iRemainingDifficulty -= uDifficultyLevel;
    enemiesToGenerate.Add(enemiesArray[uIndexToGenerate]);
  }

  spawnPool.Reset();
  int iSpawnNum = FMath::CeilToInt(enemiesToGenerate.Num() / 5) + 1;
  ensureAlways(iSpawnNum > 0);

  unsigned int spawnsAddedToPool = 0;
  while (spawnsAddedToPool != iSpawnNum)
  {
    const unsigned int uSpawnerIndex = FMath::RandRange(0, enemySpawnPoints.Num() - 1);
    AActor* pActor = enemySpawnPoints[uSpawnerIndex];
    if (!spawnPool.Contains(pActor) || spawnPool.Num() >= enemySpawnPoints.Num())
    {
      spawnPool.Add(pActor);
      spawnsAddedToPool++;

      ASpawner* pSpanwer = Cast<ASpawner>(pActor);
      if (pSpanwer)
      {
        pSpanwer->EnableSpawner(true);
      }
    }
  }
  ensureAlways(spawnPool.Num() > 0);

  enemiesAssignedToSpawn = new bool[spawnPool.Num()];
  for (int i = 0; i < spawnPool.Num(); ++i)
  {
    enemiesAssignedToSpawn[i] = false;
  }

  UE_LOG(LogTemp, Display, TEXT("Enemies: %d || Spawns: %d || Difficulty: %d"), enemiesToGenerate.Num(), iSpawnNum, _uWaveDifficulty);

  //actualiza el �ndice las primeras oleadas para controlar que en cada oleada se a�ada un enemigo nuevo de forma progresiva
  if (uArrayCapIndex < enemiesArray.Num() - 1)
  {
    uArrayCapIndex++;
  }

  UGameInstance* GameInstance = UGameplayStatics::GetGameInstance(GetWorld());
  GameInstance->GetTimerManager().SetTimer(TimerHandle, this, &UWaveManager::SpawnEnemiesTimer, fTimeBetweenEnemySpawns, true, (uCurrentWave == 1 ? 5 : fTimeBetweenWaves));
}

void UWaveManager::SpawnEnemiesTimer()
{
  ensure(isInit);
  if (bIsWaveStarting)
  {
    bIsWaveStarting = false;
    NewWaveEventDelegate.Broadcast(uCurrentWave);
  }

  if (enemiesToGenerate.Num() != 0)
  {
    //UE_LOG(LogTemp, Display, TEXT("SPAWN Wave_%d_Enemy_%d (%d)"), uCurrentWave, iSpawnedCount, enemiesToGenerate[0].DifficultyLevel);

    UWorld* pWorld = GetWorld();

    unsigned int uSpawnerIndex = 0;

    if (!allSpawnsAssigned)
    {
      allSpawnsAssigned = true;
      for (int i = 0; i < spawnPool.Num(); ++i)
      {
        allSpawnsAssigned &= enemiesAssignedToSpawn[i];
        if (!enemiesAssignedToSpawn[i])
        {
          uSpawnerIndex = i;
          enemiesAssignedToSpawn[i] = true;
          break;
        }
      }
    }

    if (allSpawnsAssigned)
    {
      uSpawnerIndex = FMath::RandRange(0, spawnPool.Num() - 1);
    }

    FString fs = FString::Printf(TEXT("Wave_%d_Enemy_%d"), uCurrentWave, iSpawnedCount);
    FActorSpawnParameters spawnParameters;
    spawnParameters.Name = FName(ToCStr(fs));

    spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

    AEnemiesBase* pSpawnedEnemy = pWorld->SpawnActor<AEnemiesBase>(enemiesToGenerate[0].Enemy, spawnPool[uSpawnerIndex]->GetTransform(), spawnParameters);
    pSpawnedEnemy->SetEnemyLevel(enemiesToGenerate[0].DifficultyLevel);
    pSpawnedEnemy->OnEnemyDeadEventDelegate.AddDynamic(this, &UWaveManager::OnEnemyDead);

    enemiesToGenerate.RemoveAt(0);

    iSpawnedCount++;
  }
  else
  {
    uCurrentWave++;
    iSpawnedCount = 0;
    enemiesToGenerate.Reset();
    bIsWaveStarting = true;

    delete[] enemiesAssignedToSpawn;
    allSpawnsAssigned = false;

    for (int i = 0; i < spawnPool.Num(); ++i)
    {
      ASpawner* pSpanwer = Cast<ASpawner>(spawnPool[i]);
      if (pSpanwer)
      {
        pSpanwer->EnableSpawner(false);
      }
    }

    UGameInstance* GameInstance = UGameplayStatics::GetGameInstance(GetWorld());
    GameInstance->GetTimerManager().ClearTimer(TimerHandle);

    NextWave();
  }
}

void UWaveManager::OnEnemyDead(AEnemiesBase* _deadEnemy)
{
  iScore += _deadEnemy->GetEnemyLevel();
  UpdateScoreEventDelegate.Broadcast(iScore);
  _deadEnemy->OnEnemyDeadEventDelegate.RemoveDynamic(this, &UWaveManager::OnEnemyDead);
}

void UWaveManager::NextWave()
{
  GenerateNextWave(uCurrentDifficultyLevel);
  uCurrentDifficultyLevel += uDifficultyIncreaseBetweenWaves;
}

void UWaveManager::CheckAndUpdateArrayCap(unsigned int* pArrayCap_, unsigned int _iRemainingDifficulty)
{
  ensure(isInit);

  unsigned int uHigherDifficultyLevel = enemiesArray[(*pArrayCap_)].DifficultyLevel;
  while (uHigherDifficultyLevel > _iRemainingDifficulty)
  {
    --(*pArrayCap_);
    uHigherDifficultyLevel = enemiesArray[(*pArrayCap_)].DifficultyLevel;
  }
}
