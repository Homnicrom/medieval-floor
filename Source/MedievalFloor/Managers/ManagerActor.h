// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelManager.h"
#include "UIManager.h"
#include "ManagerActor.generated.h"

UCLASS()
class MEDIEVALFLOOR_API AManagerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AManagerActor();

	// Declaramos el LevelManager que tendr� SIEMPRE este actor.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ULevelManager* pLevelManager;

	// Declaramos el UIManager que tendr� SIEMPRE este actor.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UUIManager* pUIManager;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
