// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../UI/MainMenuUI.h"
#include "../UI/GamePlayerUI.h"
#include "../UI/GameOverUI.h"
#include "../UI/GlobalGameUI.h"
#include "LevelManager.h"
#include "../GameMode/GameplayGameMode.h"
#include "UIManager.generated.h"

//Forward declaration
class UPauseMenuUI;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MEDIEVALFLOOR_API UUIManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUIManager();

	bool bAllPlayersReady;



private:

	ULevelManager* pLevelManager;

	//Contendr� la instancia del widget del men� principal, en caso de que el men� principal no est� presente ser� nullptr.
	UMainMenuUI* pMenuWidget;

	//Contendr� la instancia del widget del men� de pausa, en caso de que el men� de pausa no est� presente ser� nullptr.
	UPauseMenuUI* pPauseWidget;

	//Contendr� la instancia del widget de la pantalla de gam over, en caso de que no haya ser� nullptr.
	UGameOverUI* pGameOverWidget;

  UGlobalGameUI* pGlobalGameWidget;

	AGameplayGameMode* pGameMode;

	//Declaramos los BP a utilizar a la hora de crear los widgets.
	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> mainMenuBP;

	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> pauseMenuBP;

	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> gameOverBP;

  UPROPERTY(EditAnywhere)
    TSubclassOf<UUserWidget> globalGameBP;

  int iNumberPlayersReady;

  bool isInitialized;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void InitUI();

  UFUNCTION(BlueprintCallable)
	  UMainMenuUI* GetMainMenuWidget();

	UPauseMenuUI* GetPauseMenuWidget();

	UFUNCTION()
		void LoadPauseUI(bool _paused);

	UFUNCTION()
		void LoadGameOverUI(int _score, bool _isHighScore);

	UFUNCTION()
		void HideGameOverUI();

	UFUNCTION()
		void UpdatePlayer_CheckControllerUI(uint8 _playerID, bool _readyStatus);

	UFUNCTION()
		void SetAllPlayersReady();

  UFUNCTION()
    void UpdateWave_GlobalGameUI(int _waveNumber);

  UFUNCTION()
    void TemporaryInit();

  UFUNCTION()
    void UpdateScore_GlobalGameUI(int _currentScore);
};
